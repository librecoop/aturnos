
# Aturnos - notas básicas de diseño e implementación

_Punto de entrada de documentación de cambios y nuevas funcionalidades, a la espera de moverse a algo más estructurado_

## Implementación

### General - manejo de respuestas HTTP - mensaje
Comenzando con la vista de `change_password`, se define una primera aproximación al manejo de respuestas OK/ERROR de la API.

#### Back - django
- Se incluye el mensaje custom de OK/Error en el campo `message` de la respuesta JSON de la API (`response.data['message']`)

#### Front - react
- Se muestra (TODO - en un footer durante unos segundos) el texto que venga en el campo `message` de la respuesta HTTP.

### Fechas - días de la semana

- En javascript (`dayjs()`) los días de la semana van:
    - 0: Sunday
    - ...
    - 6 
- Mientras que en python (`datetime`) son:
	- 0 : Monday
	- ...
	- 6 : Sunday

Implementación:
- Se sigue el "criterio javascript" en la API:
    - (pendiente decisión si debería ser el cliente -React u otro- quien hiciese la conversión basado en las fechas de la API DRF).
- En la creación de turnos (`Shift - perform_create()`; `views.py`)
- Se resta -1 al día de la semana que viene del frontal

### Turnos : Shift

#### Cálculo de nuevas fechas y duración cuando se hace update de un turno

- Se implementa la función `perform_update` en `myapi/views.py` para el modelo `Shift`.
- Ejemplo: 
    - si para un turno de 10 a 13 se modifica `endT = 14` la hora de finalización del turno debería actualizarse (`end_time = 2:00PM`).
    - y la duración del turno pasaría a ser de 4h
    
### Usuarias

#### API

##### Creación
- Se habilita la creación de nuevas usuarias mediante API de DRF.
- Básicamente se expone el modelo `User` mediante una vista genérica (`ListCreateAPIView`) en el endpoint `/users`
- **Ejemplo**:

```JSON
{ 
    "username": "tony", 
    "first_name": "Tony",
    "is_staff": True,
    "password": "t0pS€cret", // read-only
}

```

##### Cambio de contraseña

Se incluye el id de usuaria en la url, ejemplo:
```JSON
PUT /myapi/change_password/24

{ 
    "old_password": "t0pS€cret", 
    "new_password": "€venM0R3S€cret", // read-only
}

```


#### Front
- se crean nuevas rutas con sus renders:
    - `/users` : `renderUserList` (sólo staff)
    - `/users/new` : `renderUserEditor` (sólo staff)
    - `/users/change_password` : `renderUserChangePassword` (cualquier usuaria)

- Se añade (para staff) un botón (apuntando a `/users`) en la pantalla de usuaria






