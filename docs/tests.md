
# Testing - Aturnos

## E2E

Basic end-to-end test cases written in **Cypress** are shipped to validate the app

- Cypress is saved as a dev dependency, so it is available after `npm install`

- cypress UI can be launched using:

```shell
npx cypress open
```

- Click on a test case (e.g. `basic_shifts.js`) to open a browser and run it

- Test configurations like screen size and base URL are defined in the file **`cypress.json`**

- Test cases are stored under: **`client/cypress/integration`**

- Remember to start **both server and client** before running tests ;-)

## Resources
- Cypress API docs:

https://docs.cypress.io/api/api/table-of-contents.html