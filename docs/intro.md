
# Aturnos - Documentación

_Punto de entrada a la documentación para colaborar en el desarrollo de esta aplicación_

## Pautas de diseño e implementación

**Disclaimer:** Estas pautas son meras sugerencias para facilitar el proceso de incoporación al desarrollo de esta aplicación

- Usar metodología TDD (Test Driven Development):
    - 1. Primero escribir los tests (python y/o cypress)
    - 2. Luego escribir el código
