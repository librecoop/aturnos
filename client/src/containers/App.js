import React, { Component } from 'react';

import moment from 'moment';

// Spanish Dates from Locale
import "moment/locale/es";

import axios from 'axios';
// import Cookies from 'js-cookie'
import jp from 'jsonpath'
// import _ from 'lodash'

// React router
import { BrowserRouter, 
    Switch, 
    Route, 
    Link, 
    Redirect
} from 'react-router-dom';

// History API - used for back button
import history from 'history/browser';
    
// Font Awesome Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { 
    faArrowLeft,
    faEdit,
    faPencilAlt,
    faInfoCircle,
    faPlusCircle,
    faUser,
    faThumbsUp,
    faTimes,
} from '@fortawesome/free-solid-svg-icons'

import { 
    faCalendar, 
    faCalendarAlt,
    faCalendarPlus,
} from '@fortawesome/free-regular-svg-icons'

// App styles
import './App.css';

// Setup CSRF token for axios POST/PUT/PATCH
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

/** Functional components - To be moved to another file - component **/

const Backdrop = (props) => {
    return props.show 
      ? <div className='backdrop' onClick = {props.click} /> 
      : null 
}

const Modal = (props) => {
    return <div>
      <Backdrop show={props.show} click={props.close} />
      <div  
        className='Modal'
        style = {{display: props.show ? 'block' : 'none' }}
      >
        <div 
          className = 'mybtn mybtn-circle green topright'
          onClick = {props.close}
        >x</div>
        {props.children}
      </div>
    </div>
}


/** Takes an array of labels/types 
    - Builds a table of input
    - Needs a (generic) input handler - will pass the Change event
    - Plus a button saver
    + (Optional): 
        + props.delete (true/false)
        + props.deleteHandler
        
    (Sample inputs array): 
    const inputsArr=[
        {label: 'Date', name: 'somedate', type: 'date'},
        {label: 'Week', name: 'someweek', type: 'week'},
        {label: 'Number', name: 'someno', type: 'number'},
        {label: 'Text', name: 'sometext', type: 'text'},
        {label: 'Constant', name: 'sometext', type: '_constant'},
    ]
    
    - 'name' attr is used to match corresponding state.editors.['area'] object
        In order to render a 'New' or 'Edit' screen
    
    // TODO - Auto build label based on name
    Other props:
    - saveLinkTo where
    
*/
const Editor = (props) => {
    
    console.log (`Rendering Table Editor with ${props.inputs.length} inputs`) // Debug
    
    // Render inputs array as table rows
    const rows = props.inputs.map( (inp, idx) => {
        
      // _hidden for ID values and so
      if (inp.type === '_hidden' ) {
        return null
      }
      // Json 
      let input_ele 
    
      switch (inp.type) {
        // Is a checkbox
        case 'checkbox':
            input_ele = ( <label className='simplecheck'>
                <input 
                  type = {inp.type}
                  name = {inp.name}
                  checked = {props.values[inp.name]}
                  onChange = {props.changeHandler}
                />
                <span className='box'>
                  <span className='ball'/>
                </span>
              </label>
            ) 
            break
        case '_constant':    
            // console.log(`_constant found!`)
            input_ele = <span>{inp.value}</span>
            break
        // Other than checkbox/_constant
        default:
            input_ele = ( // Non checkbox inputs
                <input 
                  type = {inp.type}
                  name = {inp.name}
                  value = {props.values[inp.name]}
                  onChange = {props.changeHandler}
                />
            )
            break
      }
      
      // const input_props = (inp.type === 'checkbox' ) 
        // ? { checked : props.values[inp.name] }
        // : 
      
      return (
        <tr key={inp.name}>
          <td>
            {inp.label}
          </td>
          <td>
            {input_ele}
          </td>
        </tr>
      )
    })
    
    // Optional delete
    const delete_btn = props.delete 
        //
        ? (
            <div
              className = 'mybtn red'
              onClick = {props.deleteHandler}
              data-cy='delete' 
            >Borrar</div>)
        //
        : null
    
    // Confirm delete modal
    const delete_modal = props.delete 
      ?   <Modal 
            show={props.show_modal}
            close={props.closeModalHandler}
          >
            <p>¿Seguro?</p>
            <div className='mybtn red' 
              onClick={props.confirmDeleteHandler}
              data-cy='confirm-delete' 
            >
              <FontAwesomeIcon icon={faTimes} /> Borrar
            </div>
          </Modal>
      : null
    
    return (
      <div>
        <table>
          <tbody>
            {rows}
          </tbody>
        </table>
        <div
          to = {props.saveLinkTo}
          className = 'mybtn green'
          onClick = {props.saveHandler} 
        >Guardar</div>
        {delete_btn}
        {delete_modal}
      </div>
    )
}

/** Takes:
    username
    shift (object)
    Handlers: 
    - 'commit' click
    - 'cancel' click
    
*/
const Application = (props) => {
    
    const shift = props.shift
    
    // Check if I am committed
    const results = shift.commitments.filter (c => {
        return c.username === props.username
    })
    
    const amIcommited = (results.length === 1)
    const cmt_id = amIcommited ? results[0].id : null
    
    console.log(`amIcommited: ${amIcommited}: cmt_id: ${cmt_id}`) 
    const commit_btn = amIcommited 
      ? <div className='mybtn mybtn-wide disabled green' // Add .disabled class
            data-cy="commit-btn"
          >Me apunto</div>
      : <Link className='mybtn mybtn-wide green'
            to="/commitments"
            data-cy="commit-btn"
            onClick = { () => props.commitClickHandler(shift.id) }
          >Me apunto</Link>  
    
    const cancel_btn = amIcommited 
      ? <Link className='mybtn mybtn-wide red'
            to="/commitments"
            data-cy="cancel-btn"
            onClick = { () => props.cancelCommitmentClickHandler(cmt_id) }
          >Anular</Link>  
      : null

    
    const disp_date = moment(shift.date).format('D MMM')
    
    return (
      <div>
        <h1>Apuntarse</h1>
        <div>
          <div>
            <h3>¡Hola {props.first_name}!</h3>
            <table className='firstRight'><tbody>
                <tr><td>Cuadrante</td><td>: <b>{shift.calendar_group}</b></td></tr>
                <tr><td>Turno    </td><td>: <b data-cy="commit-shift-id">{shift.id}</b></td></tr>
                <tr><td>Estado   </td><td>: <b>{shift.committed}/{shift.persons}</b></td></tr>
                <tr><td>Fecha    </td><td>: <b>{disp_date}</b></td></tr>
                
            </tbody></table>
            <p>De <b>{shift.startT} a {shift.endT}</b></p>
          </div>
          
          {commit_btn}
          {cancel_btn}
          
        </div>
      </div>
    )
}

const DemoComponent = (props) => {
    
    console.log(`Demo props: ${JSON.stringify(props)}`)
    const clickHandler = () => {
        console.log(`Click!`)
        console.log("Demo handler")
        props.history.push({ pathname: '/calendars/32'})
        // props.history.push( '/bab/hey')
    }
    
    return (
    <div>
        <h1>Demo mode under construction...</h1>
        <button className='mybtn mybtn-wide green' onClick = { clickHandler }>Prueba</button>
    </div>
    )
}


 

/** Core component **/
class App extends Component {
  
  // Set up state and calendar params
  constructor(props){
    
    console.log(`App.js - Constructor`)
    super(props)
    
    // Manually extract group from path
    let url_path = window.location.pathname
    console.log(`url_path: ${url_path}`)
    let org = 'supercoop' // TODO
    let logged_in = false
    
    // Get org removing first /
    // let org = url_path.split("/")[1] 
    // if (org) {
        // console.log(`org: ${org}`)
    // }
        
    
    // Check for token and update application state if required
    // localStorage.clear()
    
    const user_data = JSON.parse(localStorage.getItem('user_data'));
    if (user_data) {
        console.log(`Got user_data from localStorage!`)
        console.log(`${JSON.stringify(user_data)}`)
        logged_in = true
        org = 'supercoop'
        /** Sample
          { "user_id":3,
            "is_staff":false,
            "username":"arsenio",
            "token":"e2742fd6a5e3b0d674957c96d5623c2a67b3d0e2",
            "email":"arseruiz@gmail.com"
            // todo - org/org
          }
        
        */
        
        // this.setUser(user_data)
        // TODO - Refresh token
    }
    else {
        console.log(`got to login again`)
    }
    
    // Use for testing
    const test_state = {
        
        screen   : 'calendar',
        back_screen   : 'dashboard',
        
        userlogin : 'arsenio', 
        usergroup : 'BAB', 
        password : '', 
        
        user_data : {
            user_id : 3,
            is_staff: false,
            username : 'arsenio', 
            token: '',
            email: 'arseruiz@gmail.com',
        },
        
        calendar_id: 34, // Default
        calendars: [ {
          "id": 34,
          "org": "BAB",
          "group": "Panadería",
          "owner": "arse",
          "start_date": "2020-04-20",
          "active": true,
          "shift_count": 5,
          "shifts": [
            {
              "id": 102,
              "calendar_id": 34,
              "calendar_group": "Panadería",
              "description": "",
              "type": null,
              "day": 1,
              "date": "2020-04-20",
              "start_time": "10:00:00",
              "startT": 10,
              "end_time": "13:00:00",
              "endT": 13,
              "duration": "03:00:00",
              "persons": 3,
              "committed": 0,
              "commitments": []
            },
            {
              "id": 103,
              "calendar_id": 34,
              "calendar_group": "Panadería",
              "description": "",
              "type": null,
              "day": 2,
              "date": "2020-04-21",
              "start_time": "14:00:00",
              "startT": 14,
              "end_time": "17:00:00",
              "endT": 17,
              "duration": "03:00:00",
              "persons": 2,
              "committed": 0,
              "commitments": []
            },
            {
              "id": 104,
              "calendar_id": 34,
              "calendar_group": "Panadería",
              "description": "",
              "type": null,
              "day": 3,
              "date": "2020-04-22",
              "start_time": "10:00:00",
              "startT": 10,
              "end_time": "13:00:00",
              "endT": 13,
              "duration": "03:00:00",
              "persons": 3,
              "committed": 0,
              "commitments": []
            },
            {
              "id": 105,
              "calendar_id": 34,
              "calendar_group": "Panadería",
              "description": "",
              "type": null,
              "day": 4,
              "date": "2020-04-23",
              "start_time": "14:00:00",
              "startT": 14,
              "end_time": "17:00:00",
              "endT": 17,
              "duration": "03:00:00",
              "persons": 2,
              "committed": 0,
              "commitments": []
            },
            {
              "id": 106,
              "calendar_id": 34,
              "calendar_group": "Panadería",
              "description": "",
              "type": null,
              "day": 5,
              "date": "2020-04-24",
              "start_time": "10:00:00",
              "startT": 10,
              "end_time": "13:00:00",
              "endT": 13,
              "duration": "03:00:00",
              "persons": 3,
              "committed": 0,
              "commitments": []
            }
          ]
        }
        ],
    
        commitments: [],
        urgent_shifts: [],
        commitment_history: [],
        commitments_confirm: [],
        
    }
    
    this.state = test_state
    
    // Initial state
    this.state = {
    // let def_state = {
        
        // Req status
        loading: false, // TODO - Use load_counter
        redirect: null,
        request_error: false,
        
        error_msg: '',
        // Elements
        screen   : 'login',
        // Screen to load when pressing back button - To be replaced with React Router
        back_screen: 'dashboard',
        // screen   : 'calendar', // Use with calendar_id
        
        show_modal : false,
        show_cmt_shift : {},
        
        // User-auth params
        logged_in : logged_in,
        user_data: user_data ? user_data: {},
        
        // Params for login screen inputs
        userlogin : '', 
        password : '', 
        usergroup : org, // This is just the one selected by default - could be based in url
        
        // Calendar id to display/edit
        calendar_id: 0, 
        selected_group: '',
        
        user_list: [],

        calendars: null,
        
        commitments: null, 
        /*
            {
                "id": 7,
                "calendar_group": "Obras",
                "username": "marta",
                "shift": {
                  "id": 44,
                  "description": "",
                  "type": null,
                  "day": 4,
                  "date": "2020-02-24",
                  "start_time": "11:00:00",
                  "startT": 10,
                  "end_time": "14:00:00",
                  "endT": 14,
                  "duration": "03:00:00",
                  "persons": 3,
                  "calendar": 2
                }
              }
        ],
        */
        
        urgent_shifts: [],
        commitment_history: [],
        commitments_confirm: [],
        
        
        
        // Editors - default new values
        editors : {
            calendar : {
                group: 'Nuevo',
                start_date: moment().format('YYYY-MM-DD'),
                active : false,
                org : org,
                owner : user_data ? user_data.username : ''
            },
            shift: {
                day: 1,
                startT: 9,
                endT: 12,
                persons: 1,
            },
            user: {
                username: '',
            },
            change_password: {
                old_password: '',
                new_password: '',
            }
        },
        
    }
        
    // Bind keyboard events
    this.handleKeyPress = this.handleKeyPress.bind(this);
    
    // Define calendar params - TODO - set in DB
    this.cp = {
        
        startDay: 1,
        endDay: 7,
        startH: 8,
        endH : 24,
        minGrid: 30, // Minutes
        shift_default_duration: 3,
    }
    
    this.h_urgent = 48
    
    this.cp.hSpan = 60/this.cp.minGrid; 
    
  }
  
  // Handle keyboard events
  handleKeyPress = (e) => {
    // console.log (`Key Press! : ${e.key}`) // debug
    
    // Escape
    if (e.key === 'Escape') {
      
      if (this.state.show_modal) {
        console.log (`Got escape! Closing modal`) // debug
        // Close modals
        this.setState({show_modal: false})
      }
      
    }
    
  }
  
  async componentDidMount() {
    
    // Add keyboard listener
    document.addEventListener('keydown', this.handleKeyPress, false);
    
    console.log(`App.js - componentDidMount() `)
    
    // Load data if not there
    if (this.state.logged_in && this.state.user_data){
        
        console.log("Setting user data");
        this.setLoginData(this.state.user_data);
    }

    // Use for quick testing
    // axios.defaults.headers.common = {'Authorization': `Token e2742fd6a5e3b0d674957c96d5623c2a67b3d0e2`}
    // this.fetchInit();
    

  }
  
  componentDidUpdate () {
    // console.log(`App.js - componentDidUpdate() `)
    if (this.state.redirect) {
        console.log(`componentDidUpdate() - clearing redirection to ${this.state.redirect}`)
        this.setState({ redirect: null})
    }        
    
  }
  
  // Remove the key event listener on unmount to prevent potential errors and memory leaks
  componentWillUnmount () {
    
    console.log(`App.js - componentWillUnmount() `)
    document.removeEventListener('keydown', this.handleKeyPress, false);
  }
  
  /** ================ Aux functions ================ **/
  
  // TODO - Timed
  /** GET request for updated calendars object - from API **/
  updateCalendars = () => {
    const url = '/myapi/calendars/'
    // console.log(`Fetching calendar data from ${url}`) // debug
    axios.get(url)
      .then (response => {
        console.log(`Got ${url} API data : ${response.data.results.length} calendars`)
        console.log( response )
        console.debug(`${JSON.stringify(response.data,null,2)}`) // debug
        this.setState({
            calendars: response.data.results,
        })
      })
      .catch (error => {
        console.error(`API error! : ${error.config.url} : ${error.message}`)
      })

  }
  
  /** Generic GET request into state **/
  loadData = (url, get_params, state_area) => {
  // this.loadData ('/myapi/commitments/history/', {username: this.state.user_data.username}, 'commitment_history')
    
    console.log(`/GET url: ${url} : params : ${JSON.stringify(get_params)} : area: ${state_area}`)
    axios.get(url, {params:  get_params})
        .then (response => {
          console.log( response )
          console.log(`/GET API: ${url} : data : ${response.data.results.length} elements`)
          // console.debug(`${JSON.stringify(response.data.results,null,2)}`)
          let new_state = { loading: false }
          new_state[state_area] = response.data.results
          // console.debug(`New state: ${JSON.stringify(new_state)}`) //  debug
          this.setState( new_state ) 
        })
        .catch (error => {
          console.error(`API error! : ${url} : ${error.message}`)
          // console.log(JSON.stringify(error))
        })
    
  }
  
  patchData = (url, patch_data, state_path) => {
  // this.patchData ('/myapi/calendars/24/', {active: true}, 'calendars')
    
    console.log(`/PATCH url: ${url} : data : ${JSON.stringify(patch_data)} : area: ${state_path}`)
    axios.patch(url, patch_data)
        .then (response => {
          console.log( response )
          console.debug(`${JSON.stringify(response.data,null,2)}`)
          let new_state = { loading: false }
          
          // Update data in state
          if (state_path) {
            new_state[state_path] = this.updateStateWithData('PATCH', state_path, response.data)
          }
          console.debug(`New state:\n${JSON.stringify(new_state,null,2)}`) //  debug
          
          this.setState( new_state ) 
        })
        .catch (error => {
          console.error(`API error! : ${url} : ${error.message}`)
          // console.log(JSON.stringify(error))
        })
    
  }
  
  fetchInit = () => {
    // Load calendars
    // this.loadData ('/myapi/calendars/', {org: this.state.user_data.org}, 'calendars') // Only org - based
    this.updateCalendars() // Set state - /myapi/calendars/ - All calendars
    this.loadData ('/myapi/commitments/', {username: this.state.user_data.username, open: true}, 'commitments')
    this.loadData ('/myapi/shifts/', {username: this.state.user_data.username, urgent: true}, 'urgent_shifts')
    
    // Staff loads
    if (this.state.user_data.is_staff) {
        this.loadData ('/myapi/users/', {username: this.state.user_data.username}, 'user_list') // Only for 
    }
    
  }
          
  
  
  /** login POST request **/
  loginPost = (url, login_params) => {
    // console.log(`/POST url: ${url} : params : ${JSON.stringify(login_params)}`) // Debug
    console.log(`/POST url: ${url}`)
    axios.post(url, login_params)
        .then (response => {
          console.log( response )
          console.debug(`${JSON.stringify(response.data,null,2)}`)
            
          // Store user data - As string
          
          // TODO - Include org in django user table
          const save_org = this.state.usergroup ? this.state.usergroup : 'supercoop'
          console.log(`Saving org ${save_org}`)
          response.data.org = save_org
          
          localStorage.setItem('user_data', JSON.stringify(response.data));
          this.setLoginData(response.data)
          
        })
        .catch (error => {
          console.error(`API error! : ${url} : ${error.message}`)
          // console.log(JSON.stringify(error))
          this.setState( {
            loading: false,
            login_message : 'Invalid credentials',
          } ) 
        })
    
  }
  
  /** Set user data into state **/
  // Either from /login POST response or from localStorage
  setLoginData = ( user_data ) => {

      // Set up token in axios:
      axios.defaults.headers.common = {'Authorization': `Token ${user_data.token}`}
      console.log(`Logged in ${user_data.username} (${user_data.org})- is_staff: ${user_data.is_staff}; Token set up!`)
      
      // Show dashboard - logged_in (?)
      this.setState( {
        logged_in: true,
        loading: true,
        user_data: user_data // for re-login
        // screen : 'dashboard',
      } ) 

      // Load data
      this.fetchInit()
      
      
  }
  
  /** logout **/
  logout = ( ) => {

      // Remove axios token in axios:
      axios.defaults.headers.common = {}
      // Remove user data from localStorage
      localStorage.removeItem('user_data')
      
      // login would be shown
      this.setState( {
        logged_in: false,
        show_modal: false,
        loading: false,
        user_data: {},
        // screen : 'dashboard',
      } ) 
      console.log(`Logged out!`)

      
  }
  
  
  // Generic - Include HTTP response data in state - TODO - Use only this one
  updateStateWithData = (method, path, response_data) => {
    
    console.log(`Updating ${path} [${method}] data: ${JSON.stringify(response_data)} to state object`)  
    
    const new_substate = [...this.state[path]]
    
    // console.debug(`new_substate: ${JSON.stringify(new_substate,null,2)}`)
    
    if (method === 'POST'){
        // console.log(`Adding ${JSON.stringify(response_data)} to '${path}' object`) // debug
        // Basic push
        new_substate.push(response_data)
    }
    else if (method === 'DELETE'){
            
        console.log(`Removing [${response_data}] from '${path}' object`)
        // Find shift and its parent array using JSONPath
        const deleted_item = jp.query(new_substate, '$[?(@.id=='+response_data+')]')[0]
        
        // console.log(`Found element with JSONPath: ${JSON.stringify(deleted_item,null,2)}`) // debug
        
        // Remove element
        new_substate.splice(new_substate.indexOf(deleted_item), 1)
       
    }
    else if (method === 'PATCH'){
        
        const deleted_item_id = response_data.id
        
        console.log(`Updating [${deleted_item_id}] in '${path}' object`)
        
        // Find, remove and add
        const deleted_item = jp.query(new_substate, '$[?(@.id=='+deleted_item_id+')]')[0]
        
        // console.log(`Found element with JSONPath: ${JSON.stringify(deleted_item,null,2)}`) // debug
        
        // Replace element
        new_substate.splice(new_substate.indexOf(deleted_item), 1, response_data)
        
    }

    // console.log(`new_substate: ${JSON.stringify(new_substate,null,2)}`) // debug
    return new_substate
  }
  
  // Include HTTP response data in state calendars / shift
  updateCalsWithData = (response_data, area, method) => {
    
    console.log(`Updating ${area} [${method}] data: ${JSON.stringify(response_data)} to calendars object`)  
    
    const new_calendars = [...this.state.calendars]
    
    if (area === 'calendar'){
      if (method === 'POST'){
        // Basic push
        new_calendars.push(response_data)
      }
      else if (method === 'DELETE'){
            
        console.log(`Removing calendar [${response_data}] from calendars object`)  
        // Find shift and its parent array using JSONPath
        const deleted_cal = jp.query(new_calendars, '$[?(@.id=='+response_data+')]')[0]
        
        // console.log(`Found calendar with JSONPath: ${JSON.stringify(deleted_cal,null,2)}`) // debug
        
        // Remove Calendar
        new_calendars.splice(new_calendars.indexOf(deleted_cal), 1)
       
      }
      else if (method === 'PATCH'){
        
        const updated_cal_id = response_data.id
        
        console.log(`Updating calendar [${updated_cal_id}] from calendars object`)  
        
        // Find, remove and add
        const deleted_cal = jp.query(new_calendars, '$[?(@.id=='+updated_cal_id+')]')[0]
        
        // console.log(`Found calendar with JSONPath: ${JSON.stringify(deleted_cal,null,2)}`) // debug
        
        // Remove Calendar
        new_calendars.splice(new_calendars.indexOf(deleted_cal), 1)
       
        new_calendars.push(response_data)
        
      }
    }
    else if (area === 'shift'){
      
      if (method === 'POST'){
        const new_shift = response_data
        console.log(`Adding shift: ${JSON.stringify(new_shift)}] to calendars object`)  
        const cal_id_add = new_shift.calendar_id
        
        // Find shifts array using JSONPath
        const shifts = jp.query(new_calendars, '$[?(@.id=='+cal_id_add+')].shifts')[0]
        
        // console.log(`Found shifts with JSONPath: ${JSON.stringify(shifts,null,2)}`) // debug
        
        // Add new shift
        shifts.push(new_shift)
      
      }
      else if (method === 'PATCH'){
    
        console.log(`Updating shift: ${JSON.stringify(response_data)}] in calendars object`)  
        const updated_shift = response_data
        const id_update = response_data.id
        
        const shift = jp.query(new_calendars, '$..shifts[?(@.id=='+id_update+')]')[0]
        const shifts = jp.parent(new_calendars, '$..shifts[?(@.id=='+id_update+')]')
        
        // console.log(`Found shifts (parent) with JSONPath: ${JSON.stringify(shifts,null,2)}`) // debug
        // console.log(`Found shift with JSONPath: ${JSON.stringify(shift,null,2)}`) // debug
        
        // Remove shift
        shifts.splice(shifts.indexOf(shift), 1)
        // And add new
        shifts.push(updated_shift)
    
      }
      else if (method === 'DELETE'){
        
        const id_delete = response_data
        
        // Find shift and its parent array using JSONPath
        const shift = jp.query(new_calendars, '$..shifts[?(@.id=='+id_delete+')]')[0]
        const shifts = jp.parent(new_calendars, '$..shifts[?(@.id=='+id_delete+')]')
        
        // console.log(`Found shifts (parent) with JSONPath: ${JSON.stringify(shifts,null,2)}`) // debug
        // console.log(`Found shift with JSONPath: ${JSON.stringify(shift,null,2)}`) // debug
        
        // Remove shift
        shifts.splice(shifts.indexOf(shift), 1)
        
      }
    }
    
    // console.log(`new_calendars: ${JSON.stringify(new_calendars,null,2)}`) // debug
    return new_calendars
  }
  
  
  
  /** Aux comps render **/
  new_calendar_btn = () => {
  
    return this.state.user_data.is_staff  ? (
          <Link className='mybtn green'
            to="/calendars/new"
            data-cy="new-cal-btn"
            onClick = { this.newCalendarHandler }
          ><FontAwesomeIcon icon={faCalendarPlus} /> Nuevo Cuadrante</Link>  
         ) : null
  }
  
  /** ================= RENDER Area ================= **/
  
  // Login component - route: '/login'
  renderLogin = () => {
    // console.log("Rendering login")
    
    let loginBtnText = this.state.loading === true ? 'Entrando' : 'Entrar'
    
    const btn_classes = 'mybtn mybtn-wide green' + (this.state.loading ? ' disabled' : '')
    
    let login = (
      <div className="login-box ">
        <h1>Bienvenida</h1>
        <p>Usuario</p>
        <input 
            type="text"
            name="userlogin"
            value={this.state.userlogin}
            onChange={ (e) => this.handleInputChangeS (e) } />
        <p>Contraseña</p>
        <input 
            type="password"
            name="password"
            value={this.state.password}
            onChange={ (e) => this.handleInputChangeS (e) } />
        <br/>
        <br/>
        
        {/*
        
        <select className="select-css" 
          name="usergroup"
          value={this.state.usergroup} 
          onChange={ (e) => this.handleInputChangeS (e) } 
        >
            <option value="supercoop">Supercoop</option>
            <option value="BAB">BAB</option>
        </select>
        */}
        
        <p style={{color: 'red'}}>{this.state.login_message}</p>
        
        <button className={btn_classes} onClick = {this.loginButtonHandler} >{loginBtnText}</button>
        {/*
        <Link to="/demo">
            <button className='mybtn mybtn-wide blue' >Demo</button>
        </Link>
        */}        
        
      </div>
    )
    return login
  }
  
  
  // route: '/'
  renderDashboard = () => {
    
    const coord = this.state.user_data.is_staff  
    
    // TODO - separate dashboards
    const conf_cmt = coord 
        ? <Link 
            className='mybtn mybtn-wide green' 
            to={`confirm`}
            onClick = { this.confirmCommitmentsBtnH }
          >Confirmar turnos
         </Link>
        : null
    
    const urgent_shifts_btn = coord 
        ? null
        : <p>
          <Link className='dashboard-box green'
            to={`/urgent`}
          >{this.state.urgent_shifts.length} { this.state.urgent_shifts.length === 1 ? 'turno urgente' : 'turnos urgentes'} 
         </Link>
        </p>
        
    const manage_users_btn = coord
        ? <p> <Link 
                className='mybtn green'
                to={`/users`}
              >Gestionar usuarias </Link></p>
        : null
    
    // Select user dialog
    const user_modal =
          <Modal 
            show={this.state.show_modal}
            close={this.closeModalHandler}
          >
            <h2>{this.state.user_data.first_name}</h2>
            <p>{this.state.user_data.org}</p>
            <p>{this.state.user_data.is_staff  ? 'Coordi' : 'Usuaria'}</p>
            <p><Link to={`/users/change_password`}>Cambiar contraseña</Link></p>
            {manage_users_btn}
            <div 
              className='mybtn red' 
              data-cy='logout'
              onClick={this.logout}>
              <FontAwesomeIcon icon={faTimes} /> Cerrar sesión
            </div>
            
          </Modal>
    
    // TODO - remove - use in demo
    let isDemo=false
    const demo_handl = isDemo ? (
        <p>Rol: <label className='simplecheck'>
            <input 
              type = 'checkbox'
              name = 'isStaff '
              checked = {this.state.user_data.is_staff }
              onChange = { (e) => this.handleInputChangeS (e) }
            />
            <span className='box'>
              <span className='ball'/>
            </span>
            
        </label></p>
        )
        : true
    
    let dashboard = (
    <div>
        <h1>Inicio</h1>
        <div className='mybtn mybtn-circle green topright'
          data-cy='user-icon' 
          onClick = { this.userButtonHandler }>
            <FontAwesomeIcon icon={faUser} />
        </div>
        
        {user_modal}
        {demo_handl}
                
        { urgent_shifts_btn }
        
        <p>
          <Link className='dashboard-box green'
            to={`groups`}
            >{this.state.user_data.is_staff  ? 'Editar cuadrantes' : 'Cuadrantes' }
           </Link>
        </p>
        
        <p><Link className='dashboard-box green' 
            to={`commitments`}
          >Mis turnos</Link></p>
          
        
        { conf_cmt }
          
    </div>
    
    )
    return dashboard
    
  }
  
  // route: '/groups'
  renderGroupList = () => {
    
    // Prevent render if still loading
    if (!this.state.calendars){
        console.log("still loading");
        return (<div>Cargando</div>)
    }
    
    console.log(`Building calendar group list; Admin : ${this.state.user_data.is_staff }`)    
    
    // Get calendar groups
    // let calendar_groups = []
    // TODO - Count (open) calendars for each group
    /*
    [
        {group: Obras, calendars: 5},
        {group: Panadería, calendars: 3},
    ]
    */
        
    // Tmp
    let group_names = []
    
    this.state.calendars.forEach( (result) => {
        const cur_group = result.group
        // If contains
        if (group_names.indexOf(cur_group) !== -1) {
            // console.log(`Skipping ${cur_group}`)
            
        }
        else {
            // console.log(`Adding ${cur_group}`)
            group_names.push(cur_group)
        }
    })
    
    
    let group_list = group_names.map((g, index) => {
        // console.log(`Rendering calendar header: ${JSON.stringify(c)}`)
        let calbox = null
        
        // Display only active calendars for non-admin
        // if (this.state.user_data.is_staff  || c.active) { 
          calbox = (
            <Link className='mylink calendar-box calendar-active'
                key={index}
                to={`calendars`}
                onClick = { () => this.calendarGroupHandler (g)}
            >{g}</Link>
          )
        return calbox
        
    })
    
    
    return (
      <div>
        <h1><FontAwesomeIcon icon={faCalendarAlt} /> Cuadrantes</h1>
        <div>{group_list}</div>
        { this.new_calendar_btn() }
      </div>      
    )
  }
  
  // route: '/calendars'
  renderCalendarList = () => {
    
    // Prevent render if still loading
    if (!this.state.calendars){
        console.log("still loading");
        return (<div>Cargando</div>)
    }
    
    if (!this.state.selected_group) {
        return <Redirect to={`/groups`}/>
    }
    
    console.log(`Building calendar list; Admin : ${this.state.user_data.is_staff }; selected group: ${this.state.selected_group}`)    
    
    // Filter by group first
    let calendar_list = this.state.calendars.filter( (c) => {
        // console.log(`filtering ${c.group}`) // debug
        return c.group === this.state.selected_group
    })
    // console.log(`calendar_list: ${calendar_list.length} calendars`) // debug
        
    calendar_list = calendar_list.map((c, index) => {
        // console.log(`Rendering calendar header: ${JSON.stringify(c)}`) // debug
        let calbox = null
        
        if (this.state.user_data.is_staff  || c.active) {
          calbox = (
            <Link to={`/calendars/${c.id}`}
              className={ 'calendar-box' + (c.active ? ' green' : '') }
              onClick = { () => this.calendarHandler (c.id)}
              // clickSthg = { () => this.basicBindedHandler(param) }
              key={c.id}
            >
              <p><b>{c.group}</b><br/>
              {c.start_date}<br/>
              {c.shift_count} turnos</p>
            </Link>
          )
        }
        // Each element (map) - All for admin, only actives for
        return calbox
        
    })
    
    // Button
    return (
      <div>
        <h1><FontAwesomeIcon icon={faCalendarAlt} /> {this.state.selected_group}</h1>
        <div>{calendar_list}</div>
        { this.new_calendar_btn() }
      </div>      
    )
  }
  
  /** CORE Render: Fill up div with a table representing a calendar with shifts **/
  renderCalendar = (routeProps) => {
    
    if (!this.state.calendars || this.state.loading) {
        console.log("Still loading");
        return (<p>peeeera</p>)
    } // loading
    
    else if (this.state.calendars.length === 0) {
        console.log("No calendars found");
        return (<p>No hay cuadrantes</p>)
    } // loading
    
    console.log (`renderCalendar : routeProps :\n ${JSON.stringify(routeProps,null,2)}`)
    
    let cal_id = parseInt(routeProps.match.params.cal_id) // Router params
    if (!cal_id) {
        return <p>Invalid calendar</p>
    }
    
    // Build calendar
    console.log(`Building calendar with id: ${cal_id}`)
    
    // get selected calendar and shifts
    const calendar = this.state.calendars.filter ( (c) => {
      // console.log(`Filtering ${c.id} vs ${cal_id}`)
      return( c.id === cal_id )
    })[0] // only resul
    // console.log(`Got calendar object: ${JSON.stringify(calendar,null,2)}`) // debug
    if (calendar === undefined) {
        return <p>Calendar {cal_id} not found</p>
    }
    const shifts = calendar.shifts // Get first (and only) result
    
    // console.log(`Got shifts: ${JSON.stringify(shifts)}`)
    
    
    const is_staff = this.state.user_data.is_staff 
    
    const hSpan = this.cp.hSpan
    const nRows = (this.cp.endH - this.cp.startH) * hSpan 
    
    console.log(`Computed nRows: ${nRows}; hSpan: ${hSpan}`)
    // Slots are one array
    const days = new Array (6)
    
    let hCell = 1
    
    // Traverse through days - calculate array with label/span
    // From 0 (hour) to end day (5 is V)
    for (let d = this.cp.startDay-1; d <= this.cp.endDay; d++) {
      days[d] = new Array (nRows) // Each day is an array of "hours"
      
      for (let h=0; h<nRows; h++) {
        
        // console.log(`Adding day ${d} hour ${h}`)
        // Set hours col
        if (d === 0) {
          hCell = this.getHourCell (this.cp, h)
        }
        
        else {
          // hour
          hCell = this.getShiftCell (this.cp, shifts, d, h)
            
        }
        // console.log(`Computed [${d}][${h}] hCell: ${JSON.stringify(hCell)}`)
        days[d][h] = hCell
      }
    }
    
    // console.log(`Computed days array: ${JSON.stringify(days)}`) // debug
    
    // Aux pivot function (change rows with columns)
    const getHrow = (arr, n) => arr.map(x => x[n])
    
    // Get first row
    let row = getHrow (days, 0)
    // console.log(`First row: ${JSON.stringify(row)}`)

    let shiftRows = [] // JSX array
    
    // Now render row by row (hour by hour)
    for (let h=0; h<nRows; h++) {
        
        // Build row array for a specific hour
        row=getHrow (days, h)
        // console.log(`Processing row: ${JSON.stringify(row)}`) // debug
        
        // Map each row to an array of <td> elements
        let rowCells = row.map((p, index) => {
            let cell = null
            let isShift = false
            let shiftClasses = 'shift'
            
            let cell_key = index.toString() + h.toString()
            
            // Build cell object
            if (p.span !== 0) { // Ignore empty cells
              
              
              // Add some color for shifts (not first column)
              isShift = (index!==0 && p.span !==1 )
              
              if (isShift) { // 'Filled' cell
                // console.debug(`Building shift cell: ${JSON.stringify(p)}`)
                // Check if I am commited
                const cmt_id = this.checkMyCommitments(p.id) 
                if (cmt_id) { // I am commited
                   // console.debug(`Commited for shift: ${p.id}; cmt_id: ${cmt_id}`)
                   shiftClasses = 'shift commited' // Colour cell
                }
                
                const linkTo = this.state.user_data.is_staff ? `/shifts/${p.id}/edit` : `/shifts/${p.id}`
                // p has all details of the shift - with commitments)
                cell = (  
                    <td 
                      key = {cell_key} 
                      rowSpan = {p.span} 
                      className={shiftClasses} 
                      
                    ><Link className='mylink'
                        to = {linkTo}
                      >
                      T {p.label} <br/>
                      {p.commitments.length}/{p.persons}
                      </Link>
                      {/* Info icon if commitments are there */}
                      {p.commitments.length > 0 &&
                      <div className='shift-cmt-icon'
                            onClick={ (e) => this.shiftCmtClickHandler (e, p) }
                        >
                          <FontAwesomeIcon icon={faInfoCircle} />
                        </div>
                      }
                      
                    </td>
                )
              }
              else { // Empty cell/Hour
                
                const emptyCellContent = is_staff ? 
                      <Link className='mylink' 
                        style={{
                                display: 'block',
                                minWidth:'fill-available',
                                }}
                        to = { `/calendars/${cal_id}/new-shift` }>
                       {p.label}
                      </Link>
                      : p.label
                // Are links except first column (time)
                cell = index===0 ? (  
                    <td 
                      key={cell_key} 
                      rowSpan={p.span} 
                      onClick = { () => this.tableClickHandler (cal_id, index, h)}
                    >{p.label}
                    </td>
                  ) : ( // Otherwise has link to new shift
                    <td 
                      key={cell_key} 
                      rowSpan={p.span} 
                      onClick = { () => this.tableClickHandler (cal_id, index, h)}
                    >
                     { emptyCellContent }
                    </td>
                  )
              }
              
            }
            return cell
        })
        
        // Build the html <tr> element
        let newRow = (
            <tr key={h}>
                {rowCells}
            </tr>
        )
        
        shiftRows.push(newRow)
    }
    
    // Top right pencil icon (admin)
    const edit_cal_btn = is_staff 
        ? (
            <Link data-cy='edit-cal' 
              to = {`/calendars/${cal_id}/edit`}
              className='mybtn mybtn-circle dark-icon topright'
              onClick={this.editCalendarHandler}
            >
               <FontAwesomeIcon icon={faPencilAlt} />
            </Link>
        ) : null
        
    let shift_commitments_modal = null
    
    if (this.state.show_modal) {
      const shift = this.state.show_cmt_shift
      console.debug(`Got shift: ${JSON.stringify(shift)}`)
      shift_commitments_modal = 
          <Modal 
            show={this.state.show_modal}
            close={this.closeModalHandler}
          >
            <h2>Turno {shift.id}</h2>
            <p> Hay <b>{shift.commitments.length}/{shift.persons}</b> personas apuntadas:</p>
            <ul> {
              shift.commitments.map( (c, i) => {
                console.debug(`Mapping: ${JSON.stringify(c)}`)
                return (<li key={i}>{c.username}</li>)
                
              })
            } </ul>
          </Modal>
    }
    const disp_cal_date = moment(calendar.start_date).locale("es").format("D [de] MMMM");
    
    // Get days header
    
    // Render Calendar screen
    let newCal = (
        <div>
            <h1><FontAwesomeIcon icon={faCalendar} /> {calendar.group}</h1>
            {edit_cal_btn}
            {shift_commitments_modal}
            <p>Semana: {disp_cal_date}</p>
            <div className='Calendar white-container'>
                <table>
                  <tbody>
                    
                    { this.getWeekHeader(calendar) }
                    { shiftRows }
                    
                  </tbody>
                </table>
            </div>
        </div>
    )
    return newCal
  
  }
  
  // route: '/calendars/:cal_id/new-shift'
  // route: '/shifts/:shift_id/edit'
  renderShiftEditor = (routeProps) => { 
    
    // Prevent render if still loading
    if (!this.state.calendars){
        console.log("still loading");
        return (<div>Cargando</div>)
    }
    
    // console.log(`routeProps: ${JSON.stringify(routeProps,null,2)}`)
    if (!this.state.user_data.is_staff) {
        return <p>good try</p>
    }
    
    // Trick to handle save/delete button
    if (this.state.redirect) {
        console.log(`renderShiftEditor : redirecting to ${this.state.redirect}`)
        routeProps.history.replace(`${this.state.redirect}`)
        // return <Redirect to = {this.state.redirect} />
    }
    
    
    let cal_id = parseInt(routeProps.match.params.cal_id)
    let shift_id = parseInt(routeProps.match.params.shift_id)
    
    let shift 
    let existingShift = false
    
    if (shift_id) {
        shift = this.getShift(shift_id)
        if (shift) {
            existingShift = true
            console.log(`Rendering shift_editor : shift_id : ${shift_id}`)
        }
        else {
            return <p>Shift {shift_id} not found</p>
        }
        
        // console.log(`editors: ${JSON.stringify(shift,null,2)}`) // debug
    }
    else if (cal_id) {
        console.log(`Rendering new_shift editor: cal_id : ${cal_id}`)
    }
    // Invalid calendar
    else {
        return <p>Invalid calendar</p>
    }
    
    // console.log(`editors: ${JSON.stringify(this.state.editors,null,2)}`) // debug
    // Set editor data if not set
    // TODO - Use editor props and do not update state here
    if (shift && shift_id !== this.state.editors.shift.id) {
        console.log(`Setting shift into editors`)
        let new_editors = {...this.state.editors} // Get a copy
        new_editors.shift = shift
        this.setState({
            editors : new_editors
        })
        return <p>Loading</p>
    }
    
    
    // TODO - Calendar/shift not found
    // This would redirect
    // routeProps.history.push('/hello')
    
    
    let header, error_msg = ''
    
    if (this.state.request_error === true) { 
        // TODO - Set state
        error_msg = 'Something went baaaad :('
    }
    
    // Existing shift
    if (existingShift) { 
        console.log(`There is a shift! Rendering editor (with delete)`)
        cal_id = shift.calendar_id
        header = <h1><FontAwesomeIcon icon={faEdit} /> Turno {shift.id}</h1> 
    }
    
    // No id - Editor for "New shift"
    else {
        header = (<h1><FontAwesomeIcon icon={faCalendarPlus} /> Nuevo Turno</h1> )
    }
    
    const cal_name = shift 
        ? `${cal_id} - ${shift.calendar_group}` 
        : `${cal_id}`
        
    // Render editor - as table with <inputs>
    const inputsArr=[
        {label: 'Cuadrante', name: 'cal_id', type: '_constant', value: cal_name },
        {label: 'Día', name: 'day', type: 'number', min: 1, max: 5 },
        {label: 'De', name: 'startT', type: 'number', min: 9, max: 17 },
        {label: 'A', name: 'endT', type: 'number', min: 10, max: 18 },
        {label: 'Gente', name: 'persons', type: 'number', min: 1 },
    ]
    
    let shiftEditor = (
        <div>  
          {header}
          <Editor 
              inputs = {inputsArr}
              changeHandler = { (e) => this.handleEditorChange (e, 'shift')}
              saveHandler = { () => this.saveButtonHandler('shift') }
              deleteHandler = { () => this.setState( { show_modal: true } ) }
              confirmDeleteHandler = { () => this.deleteButtonHandler('shift') }
              saveLinkTo = {`/calendars/${cal_id}` /* back to calendar screen */  }
              // deleteLinkTo = {`/calendars/${cal_id}` /* back to calendar screen */  }
              values = {this.state.editors.shift}
              show_modal = {this.state.show_modal}
              delete = {existingShift}
              confirm = {true}
          />
          <p>{error_msg}</p>
        </div>
    )
    
    return shiftEditor
  
  }
  
  // route: '/calendars/:cal_id/edit'
  // route: '/calendars/new'
  renderCalendarEditor = (routeProps) => { 
  
    if (this.state.loading) {
        return <p> Loading... </p>
    }
    // Trick to handle save/delete button
    if (this.state.redirect) {
        console.log(`renderCalendarEditor : redirecting to ${this.state.redirect}`)
        routeProps.history.replace(`${this.state.redirect}`)
        // return <Redirect to = {this.state.redirect} />
    }
    // console.log(`routeProps: ${JSON.stringify(routeProps,null,2)}`)
    const cal_id = parseInt(routeProps.match.params.cal_id)
    
    
    let existingCal = false
    
    if (cal_id) {
        console.log(`Rendering calendar editor : cal_id: ${cal_id}`) // debug
        existingCal = true
        // Get calendar 
        const calendar = this.getCalendar(cal_id) 
        if (!calendar) {
            return <p> Calendar {cal_id} not found </p>
        }
    
        // and set into editors
        // TODO - do not update state here
        if (calendar.id !== this.state.editors.calendar.id) {
            console.log(`Setting calendar into editors`)
            const new_editors = {...this.state.editors}
            new_editors.calendar = calendar
            this.setState({
                editors : new_editors
            })
            return <p>Loading editors </p>
            
        }
        console.log(`calendar: ${JSON.stringify(calendar,null,2)}`)
    }
    else {
        console.log(`Rendering new calendar`)
        // editor data should have been already set?
    }
    
    // Build list of inputs for Editor component
    const inputsArr=[
        {label: 'Colectivo', name: 'org', type: '_constant', value: this.state.user_data.org },
        {name: 'owner', type: '_hidden', value: this.state.user_data.username },
        {label: 'Cuadrante', name: 'group', type: 'text' },
        {label: 'Semana', name: 'start_date', type: 'date'},
        {label: 'Activo', name: 'active', type: 'checkbox'},
    ]
    const header = <h1><FontAwesomeIcon icon={faEdit} /> {this.state.editors.calendar.group}</h1>
    
    return (
      <div>
        {header}
        <h3>Cuadrante</h3>
        <Editor 
          inputs = {inputsArr}
          changeHandler = { (e) => this.handleEditorChange (e, 'calendar')}
          saveHandler = { () => this.saveButtonHandler('calendar') }
          deleteHandler = { () => this.setState( { show_modal: true } ) }
          confirmDeleteHandler = { () => this.deleteButtonHandler('calendar') }
          // saveLinkTo = {`/calendars/${cal_id}` /* back to calendar screen */  }
          // deleteLinkTo = {`/calendars` /* back to calendar group screen */  }
          values = {this.state.editors.calendar}
          show_modal = {this.state.show_modal}
          delete = {existingCal}
          confirm = {true}
        />
      </div>
    )
  }
  
  // route: '/shifts/:id'
  renderUserApplication = (routeProps) => { 
    
    // Prevent render if still loading
    if (!this.state.calendars){
        console.log("still loading");
        return (<div>Cargando</div>)
    }
    console.log(`Rendering user_application:`)
    console.log(`routeProps: ${JSON.stringify(routeProps,null,2)}`)
    const shift_id = parseInt(routeProps.match.params.shift_id)
    
    // Get shift from state calendars
    const shift = this.getShift(shift_id)
    if (!shift){ // Shift not found
        return <div><p>Turno {shift_id} no encontrado</p></div>
    }
    console.log(`Shift is: ${JSON.stringify(shift, null,2)}`)
    
    // Verify my commitments - returns commitment ID or null
    // let cmt_id = this.checkMyCommitments (shift_id)
    
    return (
        <Application 
            first_name = {this.state.user_data.first_name}
            username = {this.state.user_data.username}
            shift = {shift}
            // Handlers
            commitClickHandler = {this.commitClickHandler}
            cancelCommitmentClickHandler = {this.cancelCommitmentClickHandler}
        />
    )
    
  }
  
  // route: '/commitments'
  renderCommitments = (routeProps) => { 
    
     // Prevent render if still loading
    
    if (!this.state.commitments || !this.state.calendars) {
        console.log("still loading");
        return (<div>Cargando</div>)
    }
    
    console.log(`Rendering commitments:`)
    // console.log(`routeProps: ${JSON.stringify(routeProps,null,2)}`)
    
    const cmts = this.state.commitments
    
    // Sort by date
    cmts.sort( (a,b) => {
        return a.shift.date > b.shift.date
    })
    let disp_date
    let message = cmts.length === 0 
      ? <p>No tienes turnos</p> 
      : <p>Tus turnos:</p> 
      
    const commitments = <ul> {
        cmts.map( (c, idx) => {
        // console.log(JSON.stringify(c)) // debug
        // Populate calendar name
        c.shift.calendar_group=c.calendar_group
        disp_date = moment(c.shift.date).format('D MMM')
        
        let ele = <li key={c.id}>
                    <Link className='mybtn green'
                      to={`/shifts/${c.shift.id}`}>
                    
                        <p><b>{c.calendar_group}</b> : Turno {c.shift.id}</p>
                        <p><b>{disp_date}</b> de <b>{c.shift.startT}</b> a <b>{c.shift.endT}</b></p>
                    </Link>
                  </li>
        return ele
       })
    } </ul>
    
    return (
      <div>
        <h1>Mis turnos</h1>
        <div>
          <h3>¡Hola {this.state.user_data.first_name}!</h3>
          {message}
          {commitments}
          <Link className='mybtn green'
            to = {'/history'}
            onClick = { this.historyBtnH }
            > Histórico </Link>
        </div>
      </div>
    )
  }
  
  // route: '/urgent'
  renderUrgentShifts = () => { 
    
    const shifts = this.state.urgent_shifts
    
    // Sort by date
    shifts.sort( (a,b) => {
        return a.date > b.date
    })
    let disp_date
    let message = shifts.length === 0 
      ? <p>No hay turnos pendientes en las próximas {this.h_urgent} horas</p> 
      : <p>Hay <b>{shifts.length}</b> { shifts.length === 1 ? 'turno urgente' : 'turnos urgentes'} en las próximas {this.h_urgent} horas:</p> 
      
    const list = <ul> {
      shifts.map( (s, idx) => {
        console.log(JSON.stringify(s)) // debug
        disp_date = moment(s.date).format('D MMM')
        
        const gaps = s.persons - s.committed
        // TODO - proper back to urgent_shifts
        let ele = <li key={s.id}>
                    <Link className = 'mylink dark-icon'
                      to = {`/shifts/${s.id}`}
                    >
                        <p><b>{s.calendar_group}</b> : Turno {s.id}</p>
                        <p><b>{disp_date}</b> de <b>{s.startT}</b> a <b>{s.endT}</b></p>
                        <p><b>{gaps}</b> { gaps === 1 ? 'puesto' : 'puestos'} (de {s.persons}) por cubrir</p>
                    </Link>
                  </li>
        return ele
      })
    } </ul>
    
    return (
      <div>
        <h1>Turnos Urgentes</h1>
        <div>
          {message}
          {list}
        </div>
      </div>
    )
  }
  
  // route: 'confirm_commitments'
  renderConfirmCommitments = () => { 
    
    let rem_cmts = this.state.commitments_confirm.length
    
    const cmt_list = <table><tbody>{
      
      
      this.state.commitments_confirm.map( (c, idx) => {
        // console.log(JSON.stringify(c)) // debug
        // Populate calendar name
        c.shift.calendar_group=c.calendar_group
        
      
        // Filter by date      // Skip future
        // console.log(`Filter out: ${c.shift.date}`)
        // if ( c.shift.date.isAfter(moment.now(), 'day') ) {
            // continue
            // console.log(`Filter out: ${c.shift.date}`)
            // return null
        // }
        
        console.log(`OK date: ${c.shift.date}`)
        const disp_date = moment(c.shift.date).format('D MMM')
        
        if (c.confirmed_date) {rem_cmts--}
        
        const ele = 
           <tr key={c.id} className='v-center'>
            <td>
              <div>
                (<span data-cy='cmt_id'>{c.id}</span>) <b>{c.username}</b><br/>
                <b>{c.calendar_group}</b> : Turno {c.shift.id}<br/>
                <b>{disp_date}</b> de <b>{c.shift.startT}</b> a <b>{c.shift.endT}</b>
              </div>
            </td>
            <td>
              {c.confirmed_date !== null  // Disable button if confirmed
                ? null
                : <button
                    className = 'mybtn green'
                    onClick = { () =>this.confirmCmtBtnH (c)}
                  ><FontAwesomeIcon icon={faThumbsUp} /></button>
              }
            </td>
              
          </tr>
        return ele
      })
    }</tbody></table>
    
    let cmt_msg = 'Loading...'
    if (!this.state.loading) {
      cmt_msg = rem_cmts === 0
        ? `No hay turnos por confirmar`
        : `Hay ${rem_cmts} turnos por confirmar`
    }
    
    // Get confirmed commitments
    return (
      <div>
        <h1>Confirmar</h1>
        <div>
          <h3>¡Hola {this.state.user_data.first_name}!</h3>
          {cmt_msg}
          {cmt_list}
        </div>
      </div>
    )
    
    
  }
  
  // route: 'commitment_history'
  renderCommitmentHistory = () => { 
    
    const cmt_hist = <ul> {
            this.state.commitment_history.map( (c, idx) => {
            console.log(JSON.stringify(c)) // debug
            // Populate calendar name
            c.shift.calendar_group=c.calendar_group
            const disp_date = moment(c.shift.date).format('D MMM')
            
            const ele = 
              <li key={c.id}>
                <p><b>{c.calendar_group}</b> : Turno {c.shift.id}</p>
                <p><b>{disp_date}</b> de <b>{c.shift.startT}</b> a <b>{c.shift.endT}</b></p>
              </li>
            return ele
           })
        } </ul>
    
    let cmth_msg = 'Loading...'
    if (!this.state.loading) {
      cmth_msg = this.state.commitment_history.length === 0
        ? 'No tienes turnos registrados'
        : `Tienes ${this.state.commitment_history.length} turnos registrados`
    }
    
    // Get confirmed commitments
    return (
      <div>
        <h1>Histórico</h1>
        <div>
          <h3>¡Hola {this.state.user_data.first_name}!</h3>
          <p>{cmth_msg}</p>
          {cmt_hist}
        </div>
      </div>
    )
    
    
  }
  
  // route: '/users'
  renderUserList = () => { 
    
    // For staff only
    if (!this.state.user_data.is_staff) {
        return <p>good try</p>
    }
    
    
    const user_list = <ul> {
        this.state.user_list.map( (u, idx) => {
        // console.log(JSON.stringify(u)) // debug
        return (
          <li key={u.id}>
            <p><b>{u.first_name}</b> : {u.username}</p>
          </li>
          )
       })
    } </ul>
    
    let msg = 'Loading...'
    if (!this.state.loading) {
      msg = this.state.user_list.length === 0
        ? 'No hay usuarias registradas'
        : `Hay ${this.state.user_list.length} usuarias:`
    }
    
    return (
      <div>
        <h1>Usuarias</h1>
        <div>
          <p>{msg}</p>
          {user_list}
          <Link className='mybtn green'
            to="/users/new"
            data-cy="new-user-btn"
          ><FontAwesomeIcon icon={faPlusCircle} /> Crear </Link>  
        </div>
      </div>
    )
    
    
  }
  
  // route: '/users/new'
  renderUserEditor = () => { 
    
    // For staff only
    if (!this.state.user_data.is_staff) {
        return <p>good try</p>
    }
        
    const inputsArr=[
        {label: 'Identificador', name: 'username', type: 'text'},
        {label: 'Nombre', name: 'first_name', type: 'text'},
        {label: 'Contraseña', name: 'password', type: 'password', value: 'password'},
        {label: 'Coordi', name: 'is_staff', type: 'checkbox'},
    ]
    
    let userEditor = (
        <div>  
          <h1><FontAwesomeIcon icon={faEdit} /> Usuaria</h1> 
          <Editor 
              inputs = {inputsArr}
              changeHandler = { (e) => this.handleEditorChange (e, 'user')}
              saveHandler = { () => this.saveButtonHandler('user') }
              deleteHandler = { () => this.setState( { show_modal: true } ) }
              confirmDeleteHandler = { () => this.deleteButtonHandler('user') }
              saveLinkTo = {`/users/` /* back to user list screen */  }
              values = {this.state.editors.user}
              show_modal = {this.state.show_modal}
              // delete = {true}
              confirm = {true}
          />
        </div>
    )
    
    return userEditor
  
  }
  
  // route: '/users/change_password'
  renderUserChangePassword = () => { 
    
    // Paragraph
    let error_p = this.state.request_error === true
        ? <p style={{color: 'red'}}>{this.state.error_message}</p>
        : null
        
    const success_p = this.state.success_message
        ? <p>{this.state.success_message}</p>
        : null
        
    const uid = this.state.user_data.user_id
        
    const inputsArr=[
        {label: 'whatever', name: 'id', type: '_hidden', value: uid},
        // {label: 'ID usuaria', name: 'id', type: '_constant', value: this.state.user_data.user_id },
        {label: 'Contraseña actual', name: 'old_password', type: 'password' },
        {label: 'Contraseña nueva', name: 'new_password', type: 'password' },
    ]
    
    const changepass_url = `/myapi/change_password/${uid}/`
    
    // saveButtonHandler('change_password', '/myapi/change_password/22', 'PUT')
    let changePassEditor = (
        <div>  
          <h2>Cambiar contraseña</h2> 
          <p>{this.state.user_data.username} ({this.state.user_data.user_id})</p>
          <Editor 
              inputs = {inputsArr}
              changeHandler = { (e) => this.handleEditorChange (e, 'change_password')}
              saveHandler = { () => this.saveButtonHandler('change_password', changepass_url, 'PUT')}
              saveLinkTo = {`/users/` }
              values = {this.state.editors.change_password}
              show_modal = {this.state.show_modal}
              // delete = {true}
              confirm = {true}
          />
          {success_p}
          {error_p}
        </div>
    )
    
    return changePassEditor
  
  }
    
   /**========== Aux render functions - for rendering calendars ========== **/
  
  /** return specific calendar object from state.calendars */
  getCalendar = (cal_id) => {
    console.log(`Looking for calendar ${cal_id}`)
    // traverse calendars object
    for (const c of this.state.calendars) {
        if (c.id === cal_id) {
            // console.log(`gotcha!`) //debug
            return c // breaks
        }
    }
    console.log(`Calendar ${cal_id} not found`)
    return null
  }
  
  /** return specific shift object from state.calendars */
  getShift = (shift_id) => {
    console.log(`Looking for shift ${shift_id}`)
    // traverse calendars object
    for (const c of this.state.calendars) {
      // console.log(`Checking cal ${c.id}`) // debug
      // traverse shifts in calendar
      for (const s of c.shifts) {
        // console.log(`Checking shift ${s.id}`) // debug
        if (s.id === shift_id) {
            // console.log(`gotcha!`) //debug
            return s // breaks
        }
      }
    }
    console.log(`Shift ${shift_id} not found`)
    return null
  }
  
  /** return shift values (duration, 1, 0) - label and cell span - JSON object  */
  getShiftCell = (cp, shifts, d, h) => {
    
    // Prepare output
    let cellLabel = `-`
    let cellSpan = 1
    
    const hSpan = 60/cp.minGrid
    // hspan is 2 for 30 min
    // console.log(`Computing labelspan for day ${d}; hourIndex ${h}; (${shifts.length} shifts passed)`) // debug
    
    // TODO - Use moment
    let shiftH = cp.startH + (h / hSpan)
    // console.log(`Computed shiftH: ${shiftH}`)
    // Try to find shift at this time
    const shiftsNow = shifts.filter ( (s) => {
      // console.log(`Filtering ${JSON.stringify(s)} vs ${shiftH}`)
      return s.day === d && s.startT <= shiftH && shiftH < s.endT
    })
    
    let myShift = {}
    if (shiftsNow.length === 0) {
        // console.log("No shifts found"); // debug
        // Return default
    }
    else if (shiftsNow.length === 1) { // We got one
        myShift=shiftsNow[0]
        // console.log(`Found shift for day ${d} : ${shiftH}: ${JSON.stringify(myShift)}`) // debug
        // Check if beginning times match
        if (myShift.startT === shiftH) {
            // Compute duration (hours) TODO - Use moment
            const duration = myShift.endT - myShift.startT
            cellSpan=duration * hSpan
            cellLabel=myShift.id
            // console.log(`day ${d} hour ${shiftH} is: valid (${cellSpan} cells)`) // debug
        }
        else {
            cellSpan = 0
            // console.log(`day ${d} hour ${shiftH} is: hidden`) // debug
        }
        
    }
    // More than 1 shift
    else {
        console.log ("Error finding shifts!! You have overlapping shifts")
        return null
        // TODO - Handle properly
    }
    // JSON response
    return { ...myShift, label: cellLabel, span: cellSpan}
  }
  
  /** Return hour span and value (JSON object) for first column */
  getHourCell = (cp, hourIndex) => {
    
    const hSpan = 60/cp.minGrid 
    // hspan is 2 for 30 min
    // Check reminder
    // console.log(`Computing labelspan for ${hourIndex}`) // debug
    let cellH = cp.startH + Math.floor (hourIndex / hSpan)
    let cellLabel = `${cellH} h`
    let cellSpan = hourIndex % hSpan === 0 ? hSpan : 0
    
    return { label: cellLabel, span: cellSpan}
  }
  
  /** TODO - Remove - Return [cmt_id] if the given shift is one of my commitments */
  checkMyCommitments = (shift_id) => {
    
    console.log (`Checking my commitments for shift [${shift_id}]`)
    
    let cmt_id = null
    // const found_cmt = jp.query(this.state.commitments, '$[?(shift.id=='+shift_id+')]').length
    // TODO - Learn to do with jp.
    const found_cmt = this.state.commitments.filter( (cmt, idx) => {
      return (cmt.shift.id === shift_id)
    })
    
    if (found_cmt.length > 0) {
        cmt_id = found_cmt[0].id
        console.log(`Gotcha!`)
        
    }
    console.debug (`found : ${found_cmt.length}; cmt_id : ${cmt_id}`) // debug
    
    return cmt_id
  }
  
  /** Return table header with week days  */
  getWeekHeader = (calendar) => {
    
    let start_day = calendar.start_date
    
    // Calendars should start on monday
    if (moment(start_day).day() !== 1) {
        console.warn(`WARNING: ${start_day} is not a monday`)
    }
    
    console.log(`start_day: ${start_day}`)
    // console.log(`calendar: ${JSON.stringify(calendar,null,2)}`)
    
    // Get calendar params
    const w_arr = ['L','M','X','J','V','S','D']
    let days_arr = []
    // From 0 to 4 (L-V)
    for (let i = this.cp.startDay-1; i < this.cp.endDay; i++){
            
        let day_str = moment(start_day).add(i,'d').format('D')
        days_arr.push(w_arr[i] + day_str)
    }
    
    console.log(`days_arr: ${JSON.stringify(days_arr)}`) // debug
   
    // Map to <th> elements
    let th_arr = days_arr.map((d) => {
        return <th key={d}>{d}</th>
    })
   
    const weekHeader = (
        <tr>
            <th className='hour-col'></th>
            { th_arr }
        </tr>
    )
    
    return weekHeader
    
  }
  
  
  
  
  /** ================= Handlers Area ================= **/
  
  // Button handlers
  
  // Sample basic click handler
  // clickSth = { this.sampleBasicHandler }
  sampleBasicHandler = () => {
    console.log(`Button was pressed`)
    this.setState( {
        screen : 'dashboard'
    } )  
  }
  
  // Login enter button
  loginButtonHandler = () => {
    console.log(`Login button was pressed`)
    
    if (this.state.loading){
        console.log(`Doing nothing`)
    }
    else {
        // Load
        this.setState({loading: true})
        const login_data = {
            userlogin: this.state.userlogin, 
            usergroup: this.state.usergroup, 
            password: this.state.password,
        }
        this.loginPost('/myapi/login/', login_data)
    }
  }
  
  backButtonHandler = () => {
    history.go(-1)
    // console.log(`Back button was pressed: ${this.state.back_screen}`)
    // this.setState( {
        // screen : this.state.back_screen,
        // back_screen: 'dashboard', // 2nd level
    // } )  
  }
  
  userButtonHandler = () => {
    console.log(`User button was pressed: ${this.state.back_screen}`)
    
    this.setState( {
        show_modal : true,
    } )  
  }
  
  calendarListHandler = () => {
    console.log("Groups list button was pressed")
    this.setState( {
        screen : 'groups',
        back_screen: 'dashboard',
    } )  
  }
  
  /** Show my commitments **/
  myCommitmentsBtnH = () => {
    console.log("myCommitmentsBtnH was pressed") // debug
    
    this.setState( {
        screen : 'commitments',
        back_screen: 'dashboard',
    } )
  }
  
  /** Show confirm commitments **/
  confirmCommitmentsBtnH = () => {
    
    console.log(`confirmCommitments button was pressed; `) // debug
        
    // Get open commitments
    this.loadData ('/myapi/commitments/', {open: true}, 'commitments_confirm')
    
    this.setState( {loading: true,
        // screen : 'confirm_commitments',
        // back_screen : 'dashboard',
    } ) 
    
  }
  
  /** Show history commitments **/
  historyBtnH = () => {
    
    console.log(`historyBtnH was pressed; `) // debug
        
    this.setState( {
        loading: true,
    } ) 
    
    // Get commitment history
    this.loadData ('/myapi/commitments/history/', {username: this.state.user_data.username}, 'commitment_history')
        
  }
  
  /** Add new calendar **/
  newCalendarHandler = () => {
    
    console.log(`newCalendarHandler button was pressed`)
    const new_editors = {...this.state.editors}
    new_editors.calendar.id = ''
    new_editors.calendar.owner = this.state.user_data.username // This might change the owner of a calendar..?
    new_editors.calendar.org = this.state.user_data.org // This might change the owner of a calendar..?
    this.setState( {
        editors: new_editors,
    } )  
  
  }
  
  
  
  // Sample one argument handlers - passed as arrow function:
  // onClick = { () => this.sampleClickHandler (c.id)}
  sampleClickHandler = (clicked_id) => {
    console.log(`sampleClickHandler was pressed; id: ${clicked_id}`)
    this.setState( {
        some_id: clicked_id,
    } )  
  }
  
  
  
  /** Click on a calendar group **/
  calendarGroupHandler = (clicked_group) => {
    console.log(`calendarGroupHandler was pressed; clicked_group: ${clicked_group}`)
    
    this.setState( {selected_group: clicked_group    } )
    
  }
  
  /** Click on a calendar from the list **/
  calendarHandler = (cal_id) => {
    console.log(`calendarHandler was pressed; id: ${cal_id}`)
    // Redirect is handled by <Link>
    
    // Get calendar details
    const cals_copy = [...this.state.calendars]
    const clicked_cal = jp.query(cals_copy, '$[?(@.id=='+cal_id+')]')[0]
    // console.debug(`clicked_cal: ${JSON.stringify(clicked_cal,null,2)}`) // debug
    
    // Populate calendar editor
    let new_editors = {...this.state.editors} // Get a copy
    new_editors.calendar = clicked_cal
    
    this.setState( {
        // redirect: `calendars/${cal_id}`, // Redirect to clicked calendar
        calendar_id: cal_id, // TODO - remove or use better
        editors: new_editors,
    } )  
  }
  
  /** Click on the edit calendar button **/
  editCalendarHandler = () => {
    console.log(`editCalendarHandler was pressed`)
    this.setState( (prevState, props) => {
        return {
            editors: {
                ...prevState.editors,
                calendar: {
                    ...prevState.editors.calendar,
                    owner: prevState.user_data.username,
                    id: prevState.calendar_id,
                }
            }
        }
    } )  
  }
  
  
  /** Confirm commitment button **/
  confirmCmtBtnH = (clicked_cmt) => {
    
    console.log(`confirmCommitment button was pressed; clicked_cmt_data: ${JSON.stringify(clicked_cmt)}`) // debug
    const cmt_id = clicked_cmt.id
    // Update commitment setting confirmation date and user
    this.patchData(`/myapi/commitments/${cmt_id}/`, {confirmed_date: moment(), confirmed_by: this.state.user_data.username}, 'commitments_confirm')
    
  }  
    
  /** Click on info icon of a shift - Display list of commited users **/
  shiftCmtClickHandler = (event, shift_data) => {
    
    // console.log(`shiftCmtClickHandler was pressed; clicked_shift_data: ${JSON.stringify(shift_data)}`) // debug
    event.stopPropagation()
    
    // Display s
    this.setState( {
        // screen : 'shift_commitments',
        // back_screen : 'calendar',
        show_cmt_shift : shift_data,
        show_modal : true,
    } )  
    
  } 
  
  /** Close modal/dialog **/
  /*
        <div className = 'mybtn mybtn-circle green topright'
            onClick = { this.closeModalHandler}     
        >
            X
        </div>
  */
  closeModalHandler = (event) => {
    event.stopPropagation()
    console.log(`Close modal was pressed`)
    this.setState( {
        show_modal : false,
    } )  
  }
  
  
  
  /** Click on a table cell - create shift **/
  tableClickHandler = (cal_id, clicked_day, clicked_h_key) => {
    
    console.log(`tableClickHandler was pressed; calendar: ${cal_id}, day: ${clicked_day}, clicked_h_key: ${clicked_h_key}`)
    
    // Ignore clicks on first column or non-admin
    if (clicked_day === 0 || !this.state.user_data.is_staff ) { return }
    
    // Compute new date to display
    // let new_date = moment().format('YYYY-MM-DD')
    
    let new_startT = this.cp.startH + Math.floor (clicked_h_key / this.cp.hSpan)
    let new_endT = new_startT + this.cp.shift_default_duration
    
    console.log(`Computed new shift values: day: ${clicked_day}, startT: ${new_startT}, endT: ${new_endT}`)
    
    // Load shift editor (new shift)
    this.setState( (prevState, props) => {
        return {
            screen : 'shift_editor',
            back_screen: 'calendar',
            editors : {
              shift: {
                day : clicked_day,
                calendar_id: cal_id,
                startT: new_startT, 
                endT: new_endT, 
                persons: 1,
              }
            }
        }  
    } )  
    
  }
  
  /** Click Commitment button **/
  commitClickHandler = (shift_id) => {
    
    console.log(`commitClickHandler; data: ${JSON.stringify(shift_id,null,2)}`)
    
    // Build JSON POST data
    const commit_data = {
        username: this.state.user_data.username,
        shift_id: shift_id,
    }
    
    this.setState({loading:true})
    // POST request
    axios.post('/myapi/commitments/', commit_data)
      .then( response => {
          console.log(`Status: ${response.status}`)
          // console.success( response )
          if (response.status === 201 ) {
            
            // Update calendars in state through API - TODO - Why?
            this.updateCalendars() // does set state
            // Add to my commitments
            // console.debug(`Response data:\n${JSON.stringify(response.data)}`) // Debug
            const new_commitments = this.updateStateWithData('POST', 'commitments', response.data)
            
            this.setState( {
                // Go to my commitments
                // screen : 'commitments',
                // back_screen: new_back_screen,
                commitments: new_commitments,
            } )    
          }
          else {
            console.warn(`WARN: Ignoring ${response.config.method} [${response.status}] response`)
          }
        })
      .catch (error => {
        console.error(`API error`)
        console.log(`${JSON.stringify(error)}`)
        // console.error(`API error! : ${error.config.url} : ${error.message}`)
      })
    
  }
  
  /** Click red Cancel button **/
  cancelCommitmentClickHandler = (cmt_id) => {
      
    // Send DELETE 
    if (cmt_id) {
        // Append /<id>/
        const delete_url = `/myapi/commitments/${cmt_id}/`
        console.log(`Delete URL : ${delete_url}`)
        
        axios.delete(delete_url)
        .then( response => {
            console.log(`Status: ${response.status}`)
            console.log( response )
            if ( response.status === 204 ) {
                console.log(`Deletion of commitment [${cmt_id}] was ok chachi`)
                // Update my commitments in state
                const new_commitments = this.updateStateWithData('DELETE', 'commitments', cmt_id)
                
                this.setState( {
                    commitments : new_commitments,
                })  
                
                // Update calendars in state through API
                this.updateCalendars() // Does another set state
            }
        })
        .catch( error => {
            console.log(`Error: ${error}`)
            console.log(`json: ${JSON.stringify(error,null,2)}`)
            this.setState({
                request_error: true
            })
        })
        
    }
    
      
  }
  
  
  
  
  /** ================= Binded handlers ================= **/
  // Use with:
  // onClick={ (e) => this.handleSimpleClick (e, 'shift') } />
  handleSimpleClick = (event, params) => {
    console.log(`handleSimpleClick; data: ${JSON.stringify(params)}`)
    event.stopPropagation()
    this.setState( {
        screen: 'dashboard',
    })
  
  }
  
  
  /** ================= Form handlers ================= **/
  
  // Generic editor input change handler 
  // Use with:
  // onChange={ (e) => this.handleEditorChange (e, 'shift') } />
  handleEditorChange = (event, area) => {
    const target = event.target
    const name = target.name
    let new_val = target.type === 'checkbox' ? target.checked : target.value
    if (target.type === 'number' ) { new_val = parseInt(new_val)}

    console.log(`Got '${area}' editor change data: [${name}]:${new_val}`)
        
    // Update state.editors.('area') object
    this.setState( (prevState, props) => {
      return {
        editors : {
           ...prevState.editors,
           [area]: {
               ...prevState.editors[area],
               [name]: new_val
           }
        },
        request_error: false        
      }
    });
  
    
  }
  
  // Simple - top level of state object - This should be the only one with area/path
  // TODO - Use some kind of react refs
  handleInputChangeS = (event) => {
    const target = event.target;
    const name = target.name;
    let new_val = target.type === 'checkbox' ? target.checked : target.value;
    if (target.type === 'number' ) { new_val = parseInt(new_val)}

    console.log(`Got input change data: [${name}]:${new_val}`)
        
    // Update state  object - Default level
    this.setState({
        [name]: new_val,
    })
    
  }
  
  
  /** Save button from Editor - POST/PATCH */
  // saveButtonHandler('change_password', '/myapi/change_password/22', 'PUT')
  saveButtonHandler = (area, url, method) => {
    
    console.log(`Save [${area}] button was pressed`)
    
    // Build url based in area, like /myapi/shifts/, or /myapi/calendars/
    let req_url = url ? url : `/myapi/${area}s/`
    
    // Check data from state
    let editor_data = this.state.editors[area]
    
    let req_method
    // Check if it's existing element
    if (method) {
        req_method = method
    }
    else {
        req_method = editor_data.id ? 'PATCH' : 'POST'
    }
    // Append /<id>/
    if (editor_data.id) { req_url += `${editor_data.id}/` }
    console.log(`Save: ${req_method} : ${req_url}`)
    console.log(`Prepared editor_data: ${JSON.stringify(editor_data,null,2)}`)
    
    // Send POST/PATCH/PUT
    axios({ method: req_method,
            url: req_url, 
            data: editor_data
        })
        .then( response => {
          console.log(`Status: ${response.status}`);
          console.log( response )
          if ( (req_method === 'POST' && response.status === 201 )
            || (req_method === 'PATCH' && response.status === 200 ) ){
            
            let new_calendars, new_calendar_id
            // Response data contains a new -validated- shift/calendar
            // console.log(`Response data:\n${JSON.stringify(response.data)}`); // Debug
            new_calendars = this.updateCalsWithData(response.data, area, req_method)
            
            // TODO - replace calendar_id with proper linking
            switch (area) {
              case 'calendar':
                new_calendar_id = response.data.id
                break
              case 'shift':
                new_calendar_id = response.data.calendar_id
                break
              case 'user':
                const new_user_id = response.data.id
                console.info(`New user ['${new_user_id}] created`)
                break
              default:
                console.warn(`WARN: '${area}' not found`)
            }
            // console.info(`'${area}' : moving to `)
            // And hit "back"
            let nextUrl
            if (area === 'calendar') {
                // history.go(-1)
                // console.info(`'${area}' : rendering created cal with history.replace`)
                // history.replace(`/calendars/${new_calendar_id}`)
                nextUrl = `/calendars/${new_calendar_id}`
                console.info(`Save handler '${area}' : Setting state with redirect to ${nextUrl}`)
            }
            else { //  Save Shift/User
                console.info(`Save handler (${area}): browsing with history.go(-1)`)
                history.go(-1)
            }
            
            // Add new data to state
            this.setState( {
                redirect : nextUrl,
                calendar_id : new_calendar_id,
                calendars : new_calendars,
            } )  
            
          }
          else if (req_method === 'PUT' && response.status === 200 ) { // change_password
            this.setState({
                request_error: false,
                success_message: response.data.message,
                error_message: null
            })
          }
          else {
            console.warn(`WARN: Ignoring ${req_method} [${response.status}] response`)
          }
        })
        .catch (error => {
          console.error(`API error! : ${error.config.url} : ${error.message}`)
          // console.log(JSON.stringify(error))
            this.setState({
                request_error: true,
                error_message: error.response.data.message
            })
        })
  }
  
  /** Delete button from Editor */
  deleteButtonHandler = (area) => {
    
    // Check data from state
    let id_delete = this.state.editors[area].id
    
    // Send DELETE 
    if (id_delete) {
        // Append /<id>/
        const delete_url = `/myapi/${area}s/${id_delete}/`
        console.log(`Delete URL : ${delete_url}`)
        
        // this.setState({loading:true})
        axios.delete(delete_url)
        .then( response => {
            console.log(`Status: ${response.status}`)
            console.log( response )
            if ( response.status === 204 ) {
                console.log(`Deletion of '${area}' [${id_delete}] was ok chachi`)
                // Delete shift/calendar from state object
                const new_calendars = this.updateCalsWithData(id_delete, area, 'DELETE')
                let nextUrl
                if (area === 'calendar') {
                    // Hit "back" twice - for delete-calendar
                    
                    // console.log(`Browsing with history.go(-2)`)
                    // history.go(-2)
                    nextUrl = `/groups`
                    console.log(`Delete handler (${area} Browsing with redirect to ${nextUrl}`)
                    
                    
                }
                else { // shifts
                    console.log(`Browsing with history.go(-1)`)
                    console.log(`History object: ${JSON.stringify(history)}`)
                    history.go(-1)
                    
                }
                this.setState( {
                    redirect : nextUrl,
                    loading    : false, 
                    show_modal : false,
                    calendars : new_calendars,
                } )  
                
            }
        })
        .catch( error => {
            // handle error
            console.log(`Error: ${error}`)
            console.log(`json: ${JSON.stringify(error,null,2)}`)
            this.setState({
                request_error: true
            })
        })
        
    }
    
  }
  
  
  /** ================= Main Render ================= **/
  
  render() {
    
    // Handle in-code redirections
    // if (this.state.redirect) {
      // console.log(`Got redirect to ${this.state.redirect}!`)
      // return  <BrowserRouter> <Redirect to={this.state.redirect} /> </BrowserRouter> 
    // }
    
    let back_btn = (<div 
          className='mybtn mybtn-circle green topleft'
          onClick = { this.backButtonHandler }>
            <FontAwesomeIcon icon={faArrowLeft} />
        </div>
    )
    
    let renderFn = this.renderDashboard
    // Basic logged-in logic - show Home if logged in, login screen if not
    // TODO - Use redirect + Router
    if (!this.state.logged_in) {
        renderFn = this.renderLogin
        back_btn = null
    }
    
    // const error_banner = this.state.request_error === true
        // ? <p>{JSON.stringify(this.state.error_message)}</p>
        // : null
        
    // Error/success banner will be rendered -for the moment- in each component (if necessary)
        
    return ( 
        <BrowserRouter>
          <div className='App'>
            {back_btn}
                <Switch>
                  
                  <Route path="/login" exact render = { this.renderLogin } /> 
                  <Route path="/demo" exact component = { DemoComponent }/> 
                  <Route path="/groups" exact component = { this.renderGroupList } /> 
                  <Route path="/calendars" exact component = { this.renderCalendarList } />
                  <Route path="/calendars/:cal_id/new-shift" exact component = { this.renderShiftEditor} /> 
                  <Route path="/calendars/:cal_id/edit" exact component = { this.renderCalendarEditor } /> 
                  <Route path="/calendars/new" exact component = { this.renderCalendarEditor } /> 
                  <Route path="/calendars/:cal_id" exact component = { this.renderCalendar} /> 
                  <Route path="/commitments" exact component = { this.renderCommitments } /> 
                  <Route path="/shifts/:shift_id/edit" exact component = { this.renderShiftEditor} /> 
                  <Route path="/shifts/:shift_id" exact component = { this.renderUserApplication} /> 
                  <Route path="/confirm" exact component = { this.renderConfirmCommitments } /> 
                  <Route path="/urgent" exact component = { this.renderUrgentShifts } /> 
                  <Route path="/history" exact component = { this.renderCommitmentHistory } /> 
                  <Route path="/users" exact component = { this.renderUserList } /> 
                  <Route path="/users/new" exact component = { this.renderUserEditor } /> 
                  <Route path="/users/change_password" exact component = { this.renderUserChangePassword } /> 
                  <Route path="/" exact render = { renderFn } /> 
                  <Route > 
                    <Redirect to = '/'/>
                  </Route>
                  
                </Switch>
            
          </div>
        </BrowserRouter>
    )
    
  }

}

 

export default App;



