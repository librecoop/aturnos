describe('Urgent shift tests', () => {
  
  const apiUrl = Cypress.env('apiUrl')
  
  const dayjs = require('dayjs')
  
  const day_today = dayjs()
  const day_today_str = day_today.format('YYYY-MM-DD')
  
  // next week if sunday, this week if not
  const week_date = day_today.day() === 0 
    ? dayjs().add(1,'day')
    : dayjs().add( 1-day_today.day(), 'day') // negative
  
  const week_str = dayjs(week_date).format('YYYY-MM-DD')

  let userlogin_adm = 'alvarito'
  let password_adm = 'supercoop20'
            
  let userlogin = 'arsenio'
  let password = 'supercoop20'

  let cal_id
    
  it('Create shift for tomorrow', () => {
    
    
    cy.log(`Today is ${day_today_str}; week: ${week_str}`)
    
    // Login as admin
    cy.visit('/')
    cy.get('input[name=userlogin]')
      .clear()
      .type(userlogin_adm) 
    
    cy.get('input[name=password]')
      .clear()
      .type(password_adm) 
    // Click Enter button
    cy.contains('button','Entrar').click()
    
    /** Create shift for tomorrow */
    
    // Enter admin area
    cy.contains('Editar').click()
    // group-list screen
    cy.contains('Cuadrantes')
    
    cy.log(`Create a new calendar for today (${day_today_str}): ${week_str}`)
    
    // cy.contains('Nuevo\scuadr').click()
    cy.get('a[data-cy=new-cal-btn]').click()
    
    // Create a new calendar named 'TestUrgent'
    cy.get('input[name=group]')
      .clear()
      .type('TestUrgent') 
    
    
    cy.log(`Set week to ${week_str}`)
    cy.get('input[name=start_date]')
      .type(week_str) 
    
    // Click checkbox
    cy.get('input[name=active] + .box')
      .click()
    
    // Click Save
    cy.contains('Guardar').click()
    
    // Went to calendar screen
    cy.url().should('not.include', '/calendars/new')
    // Get cal_id from url
    cy.location('pathname').then ((url_str) => {
    cy.log(`got url_str: ${url_str}`)
    cal_id = url_str.split('/')[2]
    
    
    
    cy.log(`got cal_id: ${cal_id}`)
    
    cy.get('.Calendar',  { timeout: 10000 }) // Calendar screen
    
    // TODO - Specific hour <tr>
    cy.get('td').eq(day_today.day()+1) // Sunday - 1st column
      .should('not.have.class','shift') // Empty as is a new calendar
      .click()
    
    cy.contains('Nuevo Turno' , { timeout: 10000 })

    // Create shift
    cy.log('Set persons and save')
    cy.get('input[name=persons]')
      .type('{upArrow}{upArrow}') // 2 x Arrow up
    cy.contains('Guardar').click()

    // Back to confirm list -should be one
    // cy.contains('h1', 'Confirmar')
    // cy.get('table tr')
    // .then( (rows_after) => {
      // cy.log(`Got ${rows_after.length} shifts to confirm - after`)          
    // })
    
    // And log out
    cy.visit('/')
    cy.get('[data-cy="user-icon"]').click()
    cy.get('[data-cy="logout"]').click()
    
    
    })
     
    
    })
    

  it('Check urgent shifts', () => {
            
    // Login as admin
    cy.visit('/')
    cy.get('input[name=userlogin]')
      .clear()
      .type(userlogin) 
    
    cy.get('input[name=password]')
      .clear()
      .type(password) 
    // Click Enter button
    cy.contains('button','Entrar').click()
    
    /** Go to urgent shifts */
    
    // Enter admin area
    cy.contains('urgente').click()
    
    cy.url().should('include', '/urgent')
    
    cy.get('li').should('have.length.gte', 1)
    // group-list screen
    
    // Got at least one
    
    // Click it - should have cal_id
  })

  // it.skip
  it('Deletes urgent calendar', () => {
    cy.log(`Deleting calendar [${cal_id}]`)
    cy.request('DELETE', `${apiUrl}/calendars/${cal_id}/`)

  })
      
    
})
  