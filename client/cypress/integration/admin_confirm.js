describe('Admin confirm tests', () => {
  
  it('Check confirm list, commit', () => {
    
    const apiUrl = Cypress.env('apiUrl')
    
    let userlogin = 'alvarito'
    let password = 'supercoop20'
            
    // Login
    cy.visit('/')
    cy.get('input[name=userlogin]')
      .clear()
      .type(userlogin) 
    
    cy.get('input[name=password]')
      .clear()
      .type(password) 
    // Click Enter button
    cy.contains('button','Entrar').click()
    
    // Go to cofnirm
    cy.contains('Confirm').click()
    cy.contains('h1','Confirmar')
    
    // Count calendar
    
    // Get id of the shift I am applying to
    cy.get('table tr', { timeout: 10000 }) 
      .then( (rows) => {
        
    cy.log(`Got ${rows.length} shifts to confirm`)
        
    cy.wrap(rows).first().find('span[data-cy=cmt_id]').then ( (span) => {
    
    let cmt_id = span[0].textContent
    
    cy.log(`Got cmt_id ${cmt_id}`)
    
    cy.wrap(rows).first().find('button').click()            
    
    // Unconfirm shift trhough API
    const unconfirm_data = {confirmed_date: null, confirmed_by: ''}
    cy.log(`Unconfirming commitment ${cmt_id} through API`)
    cy.log(`apiUrl: ${apiUrl}`)
    // cy.request('PATCH', `http://localhost:8000/myapi/commitments/${cmt_id}/`, unconfirm_data)
    cy.request('PATCH', `${apiUrl}/commitments/${cmt_id}/`, unconfirm_data)
 
    
    })
        
        
    // Back to confirm list -should be one
    // cy.contains('h1', 'Confirmar')
    // cy.get('table tr')
    // .then( (rows_after) => {
      // cy.log(`Got ${rows_after.length} shifts to confirm - after`)          
    // })
    // Unconfirm the given
    
        
    })
      
      
    
  })
  
})
  