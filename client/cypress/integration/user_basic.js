describe('User basic tests', () => {
    
    
  it('Check calendar, Commit, Cancel', () => {
        
    cy.visit('/')
    
    let userlogin = 'arsenio'
    let password = 'supercoop20'
            
    // Login
    cy.visit('/')
    cy.get('input[name=userlogin]')
      .clear()
      .type(userlogin) 
    
    cy.get('input[name=password]')
      .clear()
      .type(password) 
    // Click Enter button
    cy.contains('button','Entrar').click()
    
    
    cy.contains('Cuadrantes').click()
    cy.contains('h1','Cuadrantes')
    cy.url().should('include', '/groups')
    
    // Count groups and click first
    cy.get('a.calendar-box')
    .then( (groups) => {
      
    cy.log(`Got ${groups.length} groups with active calendars`)
        
    // Save first group name
    const group_name = groups[0].innerHTML
    cy.log(`First group: ${JSON.stringify(group_name)}`)
    console.log(`First group: ${JSON.stringify(group_name.text)}`)
    
    // Click on first group button
    cy.wrap(groups).first().click()

    
    // Count calendars
    cy.get('a.calendar-box')
      .then( (calendars) => {
        cy.log(`Got ${calendars.length} active calendars`)
      })
    // Open first active
    cy.get('a.calendar-box').first().click()
    
    
    // Calendar editor should not appear
    cy.get('[data-cy="edit-cal"]').should('not.exist')
      
    // Click on an empty cell
    cy.get('td:not(.shift)').eq(1).click()
      
    // Nothing should have happened
    cy.url().should('include', '/calendars')
    cy.get('div.Calendar').should('exist') // Calendar table is there
    
    // Calendar should have more than one shift
    cy.get('.shift').should('have.length.greaterThan', 1)
    
    
    
  // })
  // TODO - Split and use beforeEach
  // it ('Commit to a shift', function () {
      
      
    // Click on the first
    cy.get('.shift:not(.commited)').first().click()
    // Editor should not be displayed
    cy.contains('Borrar').should('not.exist') // Delete button
    
    cy.contains('Me apunto')
    
    // Get id of the shift I am applying to
    cy.get('[data-cy="commit-shift-id"]').then( (results) => {
      const shift_id = parseInt(results.text())
      let rgx = new RegExp(`Turno ${shift_id}`)
      
      cy.log (`Got shift_id: ${shift_id}`)
      // debugger
      
      cy.get('[data-cy=cancel-btn]').should('not.exist') // Cancel button
      
      cy.contains('Me apunto').click() // POST
      
      
      // New screen should be commitments list
      cy.contains('Mis turnos', {timeout: 10000} )
      // The new shift should be there
      cy.contains('li', rgx)
    
      // Now cancel my this commitment
      cy.log (`Cancelling commitment ${shift_id}`)
    
      cy.contains('li', rgx).click()

      // Commit Button Should be disabled
      cy.contains('Me apunto').should('have.class', 'disabled') 
      
      cy.get('[data-cy=cancel-btn]').should('exist').click() // Click Cancel
      // TODO - Ask for confirmation and click
      
      // Screen should be my commitments
      cy.contains('Mis turnos')
      
      // Cancelled commitment should not be there
      cy.contains('li', rgx).should('not.exist')
    
    })
    
    
    })
    
  })
  
  // TODO
  // it ('Check list of committed users', function () {
  
  // })
  
  
})