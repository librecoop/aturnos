describe('Admin basic tests', () => {
    
  it('Create, update, delete calendar', () => {
    
    // Use.. fixture?
    let cal_group = 'TestCal'
    let cal_group_chgd = 'TestCalChgd'
        
    let userlogin = 'alvarito'
    let password = 'supercoop20'
            
    // Login
    cy.visit('/')
    cy.get('input[name=userlogin]')
      .clear()
      .type(userlogin) 
    
    cy.get('input[name=password]')
      .clear()
      .type(password) 
    // Click Enter button
    cy.contains('button', 'Entrar').click()

    
    // Enter admin area - Edit calendars
    cy.contains('Editar').click()
    
    // Add calendar
    cy.contains('Nuevo Cuadrante').click()
    cy.contains('h1','Nuevo')
    
    cy.log('Set group, date and active')
    cy.get('input[name=group]')
      .clear()
      .type(cal_group) 
    
    // cy.pause()
    // Edit calendar
    cy.log('Set group, date and active')
    cy.get('input[name=start_date]')
      .clear()
      .type('2020-12-04') // TODO - next week
    
    // Click checkbox
    cy.get('input[name=active] + .box')
      .click()
    
    cy.contains('Guardar').click()
    
    // Verify - goes back to calendar (table)
    cy.contains('h1', cal_group)
    
        
    cy.log('Change data and update')
    // Click edit
    cy.get('[data-cy="edit-cal"]').click()
    cy.contains('h1', cal_group)
    
    // Change date and name
    cy.get('input[name=start_date]')
      .type('2020-02-21')
    
    cy.get('input[name=group]')
      .clear()
      .type(cal_group_chgd) 
    
    cy.contains('Guardar').click() // Update
    
    // Goes back to calendar
    cy.contains('h1', cal_group_chgd)
        
    cy.log(`Delete calendar ${cal_group_chgd}`)
    // Open editor
    cy.get('[data-cy="edit-cal"]').click()
    // Click delete
    cy.get('[data-cy="delete"]').click()
    
    // Modal is open - confirm
    cy.get('[data-cy="confirm-delete"]').click()
    
    // Screen is calendar-list or group-list
    cy.contains('Nuevo Cuadrante')
    
    cy.contains('a.calendar-box', cal_group).should('not.exist');
    
  })
  
  it('Create, update, delete shift', () => {
    
    let userlogin = 'alvarito'
    let password = 'supercoop20'
    
    cy.visit('/')
    cy.get('input[name=userlogin]')
      .clear()
      .type(userlogin) 
    
    cy.get('input[name=password]')
      .clear()
      .type(password) 
    // Click Enter button
    cy.contains('button','Entrar').click()

    
    // Enter admin area
    cy.contains('Editar').click()
    // group-list screen
    cy.contains('Cuadrantes')
    
    // Open 2nd calendar box
    cy.log('Load second group')
    cy.get('a.calendar-box')
      .eq(1) // Second element
      .click()
    
    // TODO - Title should match group name
    
    cy.log('Load first calendar')
    cy.get('a.calendar-box')
      .first()
      .click()
    
    cy.log('Create a new shift')
    cy.get('td')
      .eq(1) // Second td cell - Monday 9AM
      .should('not.have.class','shift') // Should be emtpy of shifts
      .click()
    cy.contains('Nuevo Turno' , { timeout: 10000 })

    cy.log('Set persons and save')
    cy.get('input[name=persons]')
      .type('{upArrow}{upArrow}') // 2 x Arrow up
    // Click Save
    cy.contains('Guardar').click()
    // Verify new shift is created
    cy.get('.Calendar',  { timeout: 10000 }) // Calendar screen
    cy.get('td').eq(1)
      .should('have.class','shift')
      
    cy.log('Edit an existing shift')
    cy.get('td').eq(1).click()
    
    cy.contains(/Turno [0-9]+/) // Edit shift screen
    // Set 8-10
    cy.get('input[name=endT]').clear()
      .type('10')
    cy.contains('Guardar').click() // Update
    
    cy.get('.Calendar',  { timeout: 10000 }) // Calendar screen
    cy.get('td').eq(1)
      .should('have.attr', 'rowspan', '4')
      .click()
      // .should('have')
    
    
    cy.contains('Borrar').click()
    // Modal is open - confirm
    cy.get('[data-cy="confirm-delete"]').click()
    
    
    cy.get('.Calendar',  { timeout: 10000 }) // Calendar screen
    cy.get('td').eq(1)
      .should('not.have.class','shift')
    
    
    // cy.log('Delete an existing shift')
    // cy.get('td')
      // .eq(1) // Second element - Monday 9AM
      // .click()
    
  })
  
  
})