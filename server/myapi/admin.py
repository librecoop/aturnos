from django.contrib import admin

# Register your models here.

from .models import Calendar, Shift

admin.site.register(Calendar)
admin.site.register(Shift)
