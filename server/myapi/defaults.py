# Script to load sample data through API
#
# Run with:
#
# python myapi/defaults.py

import requests
import json

cal_data_1={
    'id': 1, 
    'name': 'caja', 
    'owner': 'arse', 
    'active': 'true',
    'start_date': '2020-02-20',
}

response = requests.post('http://localhost:8000/myapi/calendars/', cal_data_1)
print(response.text)


cal_data_2={
    'id': 2, 
    'name': 'panaderia', 
    'owner': 'arse', 
    'active': 'true',
    'start_date': '2020-02-20',
}

response = requests.post('http://localhost:8000/myapi/calendars/', cal_data_2)
print(response.text)

# TODO - Add some shifts - properly manage Types (string/int) in Django
shift_data = {
  "day": 3,
  "startT": 9,
  "endT": 11,
  "persons": 2,
  "calendar_id": 2
}

# sh_response = requests.post("http://localhost:8000/myapi/shifts/",  shift_data)
# print(sh_response.text)

    