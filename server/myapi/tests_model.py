from django.test import TestCase
from myapi.models import Calendar

class CalendarlTestCase(TestCase):
    def setUp(self):
        Calendar.objects.create(group="first", owner="etdt")
        
    def test_calendars_basic(self):
        """New calendars are Monday to Friday by default"""
        first_cal = Calendar.objects.get(group="first")
        
        self.assertEqual(first_cal.startD, 1)
        self.assertEqual(first_cal.endD, 5)