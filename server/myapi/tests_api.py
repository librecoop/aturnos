# run with
# python manage.py test myapi
# 
# Specific test class/method
# python manage.py test myapi.tests_api.UserAPITests
# python manage.py test myapi.tests_api.UserAPITests.test_api_create_get_users
# 

from datetime import datetime

from django.utils import timezone
from django.test import TestCase
from django.urls import include, path, reverse, resolve

from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.test import APITestCase, URLPatternsTestCase


from myapi.models import *

from myapi.test_utils import *

class CalendarModelTestCase(TestCase):
    """This class defines the test suite for the calendar model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.calendar_group = "First Calendar"
        self.calendar = Calendar(group=self.calendar_group, org="etdt")

    def test_model_can_create_a_calendar(self):
        """Test the calendar model can create a calendar."""
        old_count = Calendar.objects.count()
        self.calendar.save()
        new_count = Calendar.objects.count()
        self.assertNotEqual(old_count, new_count)

class CalendarAPITests(APITestCase):
    """Test suite for the api views."""
    
    
    # urlpatterns = [
        # path('myapi/', include('myapi.urls')),
    # ]
    
    def test_no_calendars(self):
        """
        If no calendars exist, an appropriate message is displayed.
        """
        # For reverse in 1.10 - appropriate name should be defined in urls.py
        # response = self.client.get(reverse('calendar-list'))
        
        response = self.client.get('/myapi/calendars/')
        
        # print(str(response))
        # print('Get response: ' + str(response.content)) # debug
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
    
    def test_api_create_calendar_default(self):
        """Test : Create basic calendar - no date - will default to today"""
        
        new_cal = create_calendar_basic ('Test One', 'arse').data
        
        # Check start date is a monday
        start_date = datetime.datetime.strptime( new_cal['start_date'], '%Y-%m-%d')
        self.assertEqual(start_date.weekday(), 0)
    
    def test_api_create_calendar_date(self):
        
        
        calendar_data = {
            'group': 'Test', 
            'org': 'tester', 
            'owner': 'admin1',
            'start_date': '2021-12-21', # a tuesday
        }
        
        response = create_calendar_api (calendar_data)

        # Check start date is a monday
        start_date = datetime.datetime.strptime( response.data['start_date'], '%Y-%m-%d')
        self.assertEqual(start_date.weekday(), 0)
    
        
    
        
    def test_api_get_calendar(self):
        """Test the api can get a given calendar."""
        init_count = Calendar.objects.count()
        # print('init_count: ' + str(init_count)) # debug
        
        new_cal = create_calendar_basic ('Test', 'arse').data
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        # Get Calendar from DB
        calendar = Calendar.objects.get()
        
        cal_id = calendar.id
        
        # GET request - API
        response = self.client.get('/myapi/calendars/%d/' % cal_id, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        new_count = Calendar.objects.count()
        # print('new_count: ' + str(new_count)) # debug
        
        
class ShiftAPITests(APITestCase):
    """Test suite for the api views."""
    def test_api_get_shift(self):
        print('Testing basic shifts: ')
        
        new_cal = create_calendar_basic ('Test cal', 'tester').data
        
        new_cal_id = new_cal['id']
        
        print('new_cal: %s' % str(new_cal)) # debug
        # print('New cal ID: %d' % new_cal_id) # debug
        
        # Create shift - monday(1 in JS) 10 to 13th
        new_shift = create_shift_api(new_cal_id, 1, 10, 13, persons=3).data
        print('New shift: %s' % str(new_shift) ) # debug
        
        new_shift_id = new_shift['id']
        shift_date_str = new_shift['date']
        print ("shift_date_str : %s" % shift_date_str)
        shift_date = datetime.datetime.strptime( shift_date_str, '%Y-%m-%d')
        
        # date should be a monday        
        print (shift_date.weekday())
        self.assertEqual(shift_date.weekday(), 0)
        
        # update shift
        u_shift_data = update_shift_api ( new_shift_id, {"endT": 14} ).data
        
        # New time
        self.assertEqual(u_shift_data['endT'], 14)
        
        # New duration - now 10 to 14
        duration_str = u_shift_data['duration']
        duration_d = datetime.datetime.strptime(duration_str, '%H:%M:%S')
        self.assertEqual(duration_d.hour, 4 )
        
        # Update now startT
        # u_shift_data = update_shift_api ( new_shift_id, {"endT": 14} ).data
        
        
        
        
class CommitmentAPITests(APITestCase):
    """Test suite for the commitment views."""
    
    def test_api_create_get_commmitments(self):
        """Test : Create commitments, GET should return only the ones for the current user"""
        
        # Login
        User.objects.create_user('laura', 'laura@example.com', 'password')
        self.assertTrue(self.client.login(username='laura', password='password'))
        
        new_cal_id = create_calendar_basic ('Test cal', 'tester').data['id']
         
        # Add couple of shifts
        first_shift = create_shift_api(new_cal_id, 2, 10, 13, persons=3).data  # tuesday
        second_shift = create_shift_api(new_cal_id, 4, 10, 13, persons=3).data  # thursday
        
        user = User.objects.create_user('mario', 'mario@example.com', 'password')
        # Create a few commitments - TODO - Proper authentication
        create_commitment_api(first_shift['id'], 'laura')
        create_commitment_api(second_shift['id'], 'laura')
        create_commitment_api(first_shift['id'], 'diego')
        create_commitment_api(second_shift['id'], 'diego')
        
        
        # Now GET
        get_params = {
            'username': 'laura',
        }
        response = self.client.get('/myapi/commitments/', data=get_params)
        print('cmt_list: %s' % str(response)) # debug
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2) # 2 shifts for laura
        
# python manage.py test myapi.tests_api.UserAPITests.test_api_create_get_users        
class UserAPITests(APITestCase):
    """Test suite for the user POST/GET."""
    
    def test_api_create_get_users(self):
        """Test : Create users via API, check GET"""
        
        # Create staff user
        User.objects.create_user('paula', 'paula@example.com', 'password', is_staff=True)
        User.objects.create_user('sergio', 'sergio@example.com', 'password', is_staff=False)
        
        # User create/retrieve should not be allowed if not logged in
        post_params = {
            'username': 'martin',
            'first_name': 'Martin',
            'password': 'password',
        }
        response = self.client.post('/myapi/users/', data=post_params)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
        response = self.client.get('/myapi/users/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
        # 403 Forbidden if user is not staff
        
        # login
        self.assertTrue(self.client.login(username='sergio', password='password')) # not staff
        response = self.client.post('/myapi/users/', data=post_params)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
        #logout
        self.client.logout()
        
        # OK 201 for POST staff - User 'martin' is created
        self.assertTrue(self.client.login(username='paula', password='password')) # staff
        response = self.client.post('/myapi/users/', data=post_params) # POST - create martin
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        # GET with params
        response = self.client.get('/myapi/users/', data={'username':'martin'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        
        
        
        
        
        
        
        