# API Utils to seed database and modify contents

# python myapi/qt.py 
# python myapi/qt.py []

import requests
import csv
import datetime

api_url = 'http://localhost:8000/myapi/'
# api_url = 'https://demo.etdt.eu/myapi/'
print ('api_url: %s' % api_url )

token = ''

# login('arsenio', 'supercoop20')
def login (user, passwd):
    
    login_data = { "userlogin": user, "password": passwd}
    print('Logging user %s' % user)
    response = requests.post(api_url+'login/', login_data)
    # print("login POST response: %s" % str(response)) # debug
    print(str(response.text))

    if 'error' in response.json():
    # if ['error']:
        # print ("Error during login")
        return response
    
    # Set auth token - global variable
    global token
    token = response.json()['token']
    # print("login token: %s" % str(token)) # debug
    
    # TODO - properly setup headers
    # headers = {'Authorization': 'Token %s' % token }
    return response
    
# Not configured
def get_api_token (user, passwd):
    
    login_data = { "username": user, "password": passwd}
    print('Logging user %s' % user)
    response = requests.post(api_url+'api-token-auth/', login_data)
    print("api-token-auth response: %s" % str(response))
    print(str(response.text))


# create_user_api('tony', 'password', 'Tony', True, 'LaFerro')    
def create_user_api (username, password, first_name, is_staff, org):
    data = { 
        "username": username, 
        "password":password, 
        "first_name": first_name, 
        "is_staff": is_staff, 
        "org": org,
    }
    # Set auth token
    headers = {'Authorization': 'Token %s' % token }
    
    response = requests.post(api_url + 'users/', data, headers=headers)
    
    print("POST response: %s" % str(response))
    print(str(response.text))
    # Return id
    user_json = response.json()
    print("user_json: %s" % user_json) # debug
    return user_json

# def change_password_api('tony', 'password', 'newpass')
def change_password (username, old_password, new_password):
    login_resp = login (username, old_password)
    print(str(login_resp))
    
    if 'error' in login_resp.json():
    # if login_resp.json()['error']:
        return login_resp
    # get user id
    user_id = login_resp.json()['user_id']
    
    return change_password_api (user_id, old_password, new_password)


def change_password_api (user_id, old_password, new_password):
    
    # Set auth token
    headers = {'Authorization': 'Token %s' % token }
        # "Referer": api_url,
    
    client = requests.session()
    
    # response = client.get(api_url + 'users/', params={'username': 'tony'}, headers=headers)
    # print("GET response: %s" % str(response))
    # print(str(response.text))
    # print(str(response.headers))
    # user_id = response.json()['results'][0]['id']
    # print("User: [%d] : %s " % (user_id, username) )
    
       
    
    # get_url = api_url 
    user_url = api_url + 'change_password/%d/' % user_id
    
    # response = client.get(user_url)  # sets cookie
    
    # print("GET response: %s" % str(response))
    # print(str(response.text))
    
    # for x in response.headers:
        # print(x.name + '=' + x.value )
        # print(str(response.headers[x]))
        # print(str(x))
        
    # Django 1.6 and up
    # print("Client cookies: %s" % str(client.cookies))
    # csrftoken = client.cookies['x-csrf-token']
    # csrftoken = client.cookies['csrftoken']
    # print("Got csrftoken: %s" % str(csrftoken))
    
    
    data = { 
        "old_password": old_password, 
        "new_password": new_password, 
        # "csrfmiddlewaretoken": csrftoken, 
    }
    
    # data['csrfmiddlewaretoken'] = csrftoken,
    print("PUT to %s" % str(user_url))
    response = client.put(user_url, data=data, headers=headers)
    print("PUT response: %s" % str(response))
    # print(str(response.text))
    # Return id
    # user_json = response.json()
    # print("user_json: %s" % user_json) # debug
    return response

    
    
# create_calendar('BAB', 'Panadería','2020-03-16', 'arse')
def create_calendar (org, group, date, username):
    data = { 
        "org": org, 
        "group": group, 
        "start_date": date, 
        "owner": username
    }
    response = requests.post(api_url+'calendars/', data )
    print("POST response: %s" % str(response))
    print(str(response.text))
    # Return id
    post_id = response.json()['id']
    # print("post_id: %d" % post_id) # debug
    return post_id

# Patch calendar
# update_calendar (23, {"org": 'BAB'})
def update_calendar (id, data):
    # response = requests.patch(api_url+'calendars/%d/' % id, json=data)
    response = requests.patch(api_url+'calendars/%d/' % id, data)
    print("PATCH [%d] response: %s" % (id, str(response)) )
    print(str(response.text))
    
# Activate (patch) calendar
def activate_calendar(cal_id):
    update_calendar (cal_id, {"active": True})

# delete_calendar(115)
def delete_calendar (id):    
    response = requests.delete(api_url+'calendars/%d/' % id)
    print("DELETE [%d] response: %s" % (id, str(response)) )
    # print(str(response.text))

# create_shift(115,1,10, 13, persons=3)
def create_shift (cal_id, day, startT, endT, persons):
    data = {
        "calendar_id": int(cal_id), 
        "day": day, 
        "startT": startT, 
        "endT": endT,
        "persons": persons,
    }
    
    response = requests.post(api_url+'shifts/', data )
    print("POST response: %s" % str(response))
    print(str(response.text))
    # Return id
    post_id = response.json()['id']
    print("post_id: %d" % post_id) # debug
    return post_id

# get_shifts( {'urgent': True} )
def get_shifts (params):
    
    response = requests.get(api_url+'shifts/', params)
    print("GET response: %s" % str(response))
    print(str(response.text))
    # Return JSON
    return response.json()

# update_shift (23, {"endT": 11})
def update_shift (id, data):
    
    headers = {'Authorization': 'Token %s' % token }
    # print ("headers are: %s" % (str(headers))) # debug
    
    response = requests.patch(api_url+'shifts/%d/' % id, data, headers=headers)
    print("PATCH [%d] response: %s" % (id, str(response)) )
    print(str(response.text))

    
# delete_shift(112)
def delete_shift (id):
    headers = {'Authorization': 'Token %s' % token }
    print ("headers are: %s" % (str(headers))) # debug
    
    response = requests.delete(api_url+'shifts/%d/' % id, headers=headers)
    print("DELETE [%d] response: %s" % (id, str(response)) )
    # print(str(response.text))



# create_commitment (32, 'marta')
def create_commitment (shift_id, username):

    data = {
        "shift_id": int(shift_id),
        "username": username
    }
    
    # Set auth token
    headers = {'Authorization': 'Token %s' % token }
    
    response = requests.post(api_url+'commitments/', data, headers=headers)
    print("POST response: %s" % str(response))
    print(str(response.text))
    # Return id
    post_id = response.json()['id']
    print("post_id: %d" % post_id) # debug
    return post_id

# get_commitments ( {'username': 'adrian'} )
def get_commitments (params):
    
    headers = {'Authorization': 'Token %s' % token }
    response = requests.get(api_url+'commitments/', params, headers=headers)
    print("GET response: %s" % str(response))
    print(str(response.text))
    # Return JSON
    return response.json()
    
# unconfirm_cmt(1)    
def unconfirm_cmt (cmt_id):
    
    print("Unconfirming commitment: %s" % str(cmt_id))    
    # (Un)Confirm commitment (patch)
    # cmt_id = 85
    # Confirm:
    # conf_data = { "confirmed_by":"marta", "confirmed_date": "2020-03-08T06:58:39.382Z" }
    # Unconfirm:
    conf_data = { "confirmed_by": "", "confirmed_date": ""}

    response = requests.patch(api_url+'commitments/%d/' % cmt_id, conf_data)

    print("PATCH response: %s" % str(response))
    print(str(response.text))


# TODO - Create user and save to the database - via API


    
# Convert CSV with headers into string Dict object
# lines = load_csv ('sample.csv', delimiter=';')
def load_csv (filename, delimiter=';'):
    lines = []
    headers = []
    line = {}
    with open(filename) as csvfile:
        # readCSV = csv.reader(csvfile, delimiter=delimiter)
        readCSV = csv.DictReader(csvfile, delimiter=delimiter)
        
        # headers_row = readCSV[0]
        # print('headers : ' + headers_row)
        for row in readCSV:
            # Skip commented lines
            # print('Appending: ' + str(row))
            lines.append(row)
            
            # print(row[0],row[1],row[2],)
        
    return lines
    
# load_by_api ('http://localhost:8000/myapi/users/', 'sample.csv', delimiter=';')
def load_by_api ( api_url, filename, delimiter=';'):
    
    # Config param
    max_cons_fail = 5
    
    lines = load_csv (filename, delimiter)
    n_ok = 0
    n_ko = 0
    
    cons_fail = 0 
    
    
    for line in lines:
        # print('Posting: ' + str(line))
        response = requests.post(api_url, line )
        code = response.status_code
        print('[%d] : %s' %(code, str(line)) )
        
        # Print details on bad requests
        if (code == 400):
            print("Bad request: %s" % (str(response.text) ) )
        if (code == 404 | code == 405):
            n_ko+=1
            cons_fail+=1
            print("Failed (%d): %s" % (code, str(line['mail']) ) )
            continue
        
        if (code == 201):
            cons_fail = 0 # reset
            n_ok += 1
            # print(str(response.text))
        
        if (cons_fail >= max_cons_fail):
            print ('Exiting after %d consecutive failures' % cons_fail)
            break
    
    
    print ('OK: %d; KO: %d' % (n_ok, n_ko) )

# =================== 2nd Order =================== #
def create_std_cal (org, group, date, username):
    
    # create calendar first
    cal_id = create_calendar(org, group, date, username)
    
    # Add shifts to calendar
    m_shift1 = create_shift(cal_id,1,10,13,3)
    t_shift2 = create_shift(cal_id,2,14,17,2)
    w_shift1 = create_shift(cal_id,3,10,13,3)
    j_shift2 = create_shift(cal_id,4,14,17,2)
    f_shift1 = create_shift(cal_id,5,10,13,3)
    
    # Activate calendar
    activate_calendar(cal_id)

    
    
# Basic data required for demo / tests
def seed_basic_data ():
    
    # Basic STD Calendar
    # For this week
    date_today = datetime.datetime.today()
    x = date_today.strftime("%Y-%m-%d")
    
    print('next_monday date: ' + x)
    
    # cal_id = create_std_cal('etdt', 'Barra', x, 'arse')
    cal_id = create_calendar('etdt', 'Talleres', x, 'arse')
    
    create_commitment (12, 'arsenio')
    