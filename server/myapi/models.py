from django.db import models

import datetime
from django.utils import timezone

class Calendar(models.Model):
    
    """User to which the calendar belongs to"""
    owner = models.CharField(max_length=32)
    # TODO - Should be relation to organization
    org = models.CharField(max_length=32)
    
    group = models.CharField(max_length=64)
    
    active = models.BooleanField('Active', default=False)
    
    start_date = models.DateField('Start Week', default=datetime.date.today)
    """Should be a monday"""
    
    """Start/End dates should be between Monday(1) and Sunday (7) """
    startD = models.IntegerField('Start Day', default=1)
    endD = models.IntegerField('End Day', default=5)

    class Meta:
        """Calendars starting same week should have different names"""
        constraints = [
            models.UniqueConstraint(fields=['group', 'start_date'], name='unique_name_date')
        ]
        
    def __str__(self):
        return self.group
    
# - ID Cuadrante (foreign key)
# - Day
# - Start time
# - End time
# - Duration (minutes)
# - Description (could be a task)
# - People required
class Shift(models.Model):
    calendar = models.ForeignKey(Calendar, related_name='shifts', on_delete=models.CASCADE)
    description = models.CharField('Description', max_length=128, blank=True)
    type = models.CharField('Calendar Type', max_length=32, null=True)
    """Day of the week"""
    day = models.IntegerField('Shift day', default=1)
    date = models.DateField('Shift date', default=datetime.date.today)
    start_time = models.TimeField('Start time', default=datetime.time(9))
    startT = models.IntegerField('Start hour', default=9)
    end_time = models.TimeField('End time', default=datetime.time(12))
    endT = models.IntegerField('End hour', default=12)
    duration = models.DurationField('Duration', default=datetime.timedelta(hours=3))
    persons = models.IntegerField('Required persons', default=1)
    
    def __str__(self):
        # return (str(self.date) + ' - ' + self.description)
        return ('[%s] %s' % ( self.id, str(self.date) ) )
        
class Commitment(models.Model):
    shift = models.ForeignKey(Shift, related_name='commitments', on_delete=models.CASCADE)
    # userlogin = models.CharField('Userlogin', max_length=128, blank=False)
    username = models.CharField('Username', max_length=128, blank=False)
    confirmed_date = models.DateTimeField('Confirmed date', null=True)
    confirmed_by = models.CharField('confirmed_by', max_length=128, blank=True)
    
    class Meta:
        """Can commit to a shift only one time"""
        constraints = [
            models.UniqueConstraint(fields=['username', 'shift'], name='unique_user_shift')
        ]

class Organization(models.Model):
    
    name = models.CharField('Organization name', max_length=32)
    type = models.CharField('Organization Type', max_length=32, null=True)
    
    def __str__(self):
        return self.name
    