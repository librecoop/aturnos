# Django script tools - to be executed with django shell

# python myapi/dt.py 
# python myapi/dt.py []

# From django (penv):
# ./manage.py shell < myapi/dt.py

import requests
import csv

from django.contrib.auth.models import User


# Functions area (To be moved to utils):
def create_user (username, password, is_staff):
    new_user = User(username = username, first_name = username, password = password, is_staff = is_staff)
    new_user.set_password(password)
    new_user.save()
    print ('User created: %s' % username )

def set_user_password (username, password):
    user = User.objects.get(username=username)
    print ("Setting password for user [%d] %s - %s" % (user.id, user.username, user.first_name))
    user.set_password(password)
    user.save()

def delete_user (username):
    user = User.objects.get(username=username)
    print ("deleting user [%d] %s - %s" % (user.id, user.username, user.first_name))
    user.delete()

# create_user ("demoadmin", "password", True)
# create_user ("demouser", "password", False)
# delete_user("tony")

set_user_password("tony", "newpass")


