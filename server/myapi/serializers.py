# serializers.py

from rest_framework import serializers

from .models import *
from django.contrib.auth.models import User

# Used to display children in higher-order APIs (calendars/, shifts/)
class CommitmentSerializer_Nest(serializers.ModelSerializer):
    
    class Meta:
        model = Commitment
        fields = ('id', 'username', 'shift_id')
        
class ShiftSerializer(serializers.ModelSerializer):
    
    # Get list of commitments for each shift
    commitments = CommitmentSerializer_Nest(many=True, read_only=True)
    
    # And a count of people commited
    committed = serializers.SerializerMethodField()
    
    # And calendar group
    calendar_group = serializers.SerializerMethodField()
    
    class Meta:
        model = Shift
        # All fields plus commitments and committed count
        fields = ('id', 'calendar_id', 'calendar_group', 'description', 'type', 'day', 'date', 'start_time', 'startT', 'end_time', 'endT', 'duration', 'persons', 'committed', 'commitments')
    
    def get_committed(self, obj):
        return obj.commitments.count()
    
    def get_calendar_group(self, obj):
        return str(obj.calendar)
    
# For standard /commitments/ endpoint
class CommitmentSerializer(serializers.ModelSerializer):
    
    # And calendar group
    calendar_group = serializers.SerializerMethodField()
    
    class Meta:
        model = Commitment
        fields = ('id', 'username', 'confirmed_date', 'confirmed_by', 'calendar_group', 'shift')
        # exclude = []
        depth = 1 # Include details of the shift on the API

    def get_calendar_group(self, obj):
        return obj.shift.calendar.group
    
    
class CalendarSerializer(serializers.ModelSerializer):
    
    # Use shifts serializer to display bot shifts and commitments for each calendar
    shifts = ShiftSerializer(many=True, read_only=True)
    
    shift_count = serializers.SerializerMethodField()
    
    class Meta:
        model = Calendar
        # All fields plus shifts
        fields = ('id', 'org', 'group', 'owner', 'start_date', 'active', 'shift_count', 'shifts')
        # exclude = []
        
    def get_shift_count(self, obj):
        return obj.shifts.count()

    

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'password', 'is_staff')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

class ChangePasswordSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('old_password', 'new_password')

    def validate_old_password(self, value):
        print("Inside validate_old_password!")
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError({"old_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):

        # print("Inside serializer update!")
        instance.set_password(validated_data['new_password'])
        instance.save()
        return instance
