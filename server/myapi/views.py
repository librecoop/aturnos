from rest_framework import viewsets
from rest_framework import generics

from .serializers import *
from .models import *

import datetime
from django.utils import timezone
from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache

    
# Auth
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.admin.views.decorators import staff_member_required

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAdminUser

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.authtoken.models import Token


# App Settings
# from settings.default import env_settings
import pprint
import django
from django.conf import settings

from xmlrpc import client as xmlrpclib

# Serve Single Page Application
index = never_cache(TemplateView.as_view(template_name='index.html'))

# Login
@api_view(["POST"])
def login(request):
    userlogin = request.data.get("userlogin")
    usergroup = request.data.get("usergroup")
    password = request.data.get("password")

    print ("Logging (%s) user %s" % (usergroup, userlogin) )
    
    user = {}
    
    # Should be set in DB based on group config
    
    # Check external login
    try:
        settings.EXTERNAL_LOGIN
    
    # Default: DRF Token Authentication
    except:
        print ("Using DRF Token Authentication")
        user = authenticate(username=userlogin, password=password)

    else :
        if (usergroup == 'supercoop'): # Authenticate vs Odoo instance (XMLRPC)
            
            url = settings.EXTERNAL_LOGIN['URL']
            odoo_db = settings.EXTERNAL_LOGIN['ODOO_DB']
            # Connect through API
            try:
                common      = xmlrpclib.ServerProxy('{}/xmlrpc/2/common'.format(url))
                models      = xmlrpclib.ServerProxy('{}/xmlrpc/2/object'.format(url))
                uid         = common.login(settings.EXTERNAL_LOGIN['ODOO_DB'], userlogin, password)
                # user         = common.authenticate(settings.ODOO_DB, userlogin, password)
                if not uid:
                    print ('Odoo said invalid credentials')
                    return Response({"error": "Odoo Login failed"}, status=HTTP_401_UNAUTHORIZED)
                    
                print ('Connected to %s, got user id: %s' % (url, uid) )
                
                # Get user details (name)
                odoo_user = models.execute( odoo_db, uid, password
                  , 'res.users', 'search_read'
                # Where
                  , [['login', '=', userlogin]]
                # Fields
                  , ['login', 'name']
                    )[0]

                pprint.pprint(odoo_user)
                
                # Split odoo full name
                full_name_arr = str(odoo_user['name']).split(' ', 1)
                
                first_name = full_name_arr[0]
                last_name = full_name_arr[1] if len(full_name_arr) ==2 else ''
                
                print ('Got %s - %s' % (first_name, last_name) ) 
                # Check user in django auth table
                user = authenticate(username=userlogin, password=password)
                
                # TODO - Auto-Update password if user exists
                
                if not user:
                    # Create (django) user if not exists
                    print('Adding user %s' % (first_name) )
                    user = User.objects.create_user(
                        username=userlogin, 
                        first_name=first_name, 
                        last_name=last_name, 
                        email=userlogin,
                        password=password)
                        # TODO - Identify admins/leaders
                else:
                    print ('User %s already exists' % (userlogin) ) 
                    # Should we update?
                    user = authenticate(username=userlogin, password=password)
                
            except:
                print ('Error connecting to Odoo')
                # sys.exit()
    
    # Common
    if not user:
        return Response({"error": "Login failed"}, status=HTTP_401_UNAUTHORIZED)

    else:
        print ("Got user object:")
        print (user)
        token, _ = Token.objects.get_or_create(user=user)
        print ("Token: %s" % (token) )
        return Response({
            'user_id': user.pk,
            'email': user.email,
            'username': user.username,
            'first_name': user.first_name,
            'is_staff': user.is_staff,
            "token": token.key
        })
        # return Response({"user_id": user})

# Create / retrieve users
class UserAPIView(generics.ListCreateAPIView):
    
    permission_classes = [IsAdminUser]  # user.is_staff is True 
    
    serializer_class = UserSerializer
    # queryset = User.objects.all().order_by('-id') # order_by to avoid pagination warning
    
    # Allow url query params filter by 'username'
    def get_queryset(self):    
        
        queryset = User.objects.all()
        
        username = self.request.query_params.get('username')
        if username is not None:    
            queryset = queryset.filter(username=username)
        
        return queryset.order_by('-id') # order_by to avoid pagination warning
    
class ChangePasswordAPIView(generics.UpdateAPIView):
# class ChangePasswordAPIView(generics.RetrieveUpdateDestroyAPIView):
    
    permission_classes = (IsAuthenticated,)
    # permission_classes = [IsAdminUser]  # user.is_staff is True 
    # permission_classes = ()
    # authentication_classes = () # Will default to SessionAuthentication - requires CSRF
    
    serializer_class = ChangePasswordSerializer
    def get_queryset(self, **kwargs):
        
        queryset = User.objects.all()
        print ("Got request:")
        print (str(self.request))
        
        # Get token
        print ("Request auth: %s" % str(self.request.auth)) # token
        print ("Authorization: %s" % str(self.request.headers['Authorization']))
        
        # User id in url
        user_id = self.kwargs.get('pk')
        user = User.objects.get(pk=user_id)
        print ("user_id: %d" % user_id)
        print ("User: %s" % str(user))
        # print (str(self.request.GET['pk']))
        
        print (str(self.request.data)) # querydict
        print ("Username (auth): %s" % str(self.request.user.username))
        # return queryset.filter(username=self.request.user.username)
        return queryset.filter(id=user_id)

    # Add {'message': '[String]'} to PUT response
    def finalize_response(self, request, response, *args, **kwargs):
        
        # print ("inside change_password finalize_response() : ")
        print(str(response)) # debug
        
        if response.exception: # boolean
            
            print("Response has exception:")
            print(str(response.data))
            
            # Invalid password
            if 'old_password' in response.data:
                
                resp_err_msg = str(response.data['old_password']['old_password'])
                # print("Got: " + resp_err_msg)
                response.data['message'] = "Error: %s" % resp_err_msg
        
        else: # Change pass success 
            response.data['message'] = "Contraseña cambiada con éxito"
        
        return super().finalize_response(request, response, *args, **kwargs)
        
        
    
class CalendarListView(generics.ListCreateAPIView):
    
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    
    serializer_class = CalendarSerializer
    # queryset = Calendar.objects.all().order_by('name')
    
    def get_queryset(self):
        """
        If 'owner' parameter is present in the URL,
        return only filtered calendars, ordered by "newest first".
        """
        
        queryset = Calendar.objects.all() # Default (QuerySets are lazy)
        
        org = self.request.query_params.get('org', None)
        if org is not None:
            queryset = queryset.filter(org=org)
        
        owner = self.request.query_params.get('owner', None)
        if owner is not None:
            queryset = queryset.filter(owner=owner)
        return queryset.order_by('-start_date')

    def perform_create(self, serializer):
        calendar_req_data = self.request.data # type dict
        print ("Calendar perform_create : \n%s" % calendar_req_data)
        
        
        calendar_data = serializer.validated_data # OrderedDict - mutable
        
        # Check date - save monday
        if not 'start_date' in calendar_data:
            start_monday = get_monday(datetime.date.today())
        
        else: 
            print ("Validating start_date")
            start_monday = get_monday(calendar_data['start_date'])
        
        calendar_data['start_date'] = start_monday
        
        serializer.save(**calendar_data)
        
        
def get_monday(start_date):
    
    # Parse date if string
    if (isinstance (start_date, str)):
        start_date = datetime.datetime.strptime(start_date,"%Y-%m-%d") 
        
    monday_date = start_date - datetime.timedelta(days=start_date.weekday())
    # print ("Monday from date %s is : %s" % ( str(start_date), str(monday_date) ) ) # debug
    return monday_date
        
class CalendarDetailView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
        

# Aux methods

# startT and endT are int
def compute_shift_data(startT, endT):
    
    # print ("Computing shift_data: %s - %s" % (startT, endT) )
    
    # From int to strings to datetimes
    shift_start_hour = datetime.datetime.strptime(str(startT),"%H") 
    shift_end_hour   = datetime.datetime.strptime(str(endT),"%H") 
    
    # Duration (timedelta)
    shift_duration = shift_end_hour - shift_start_hour
    
    # Convert to hours (datetime.time)
    shift_start_time = shift_start_hour.time()
    shift_end_time = shift_end_hour.time()
    
    # print ("Computed start/end times : %s - %s" % ( shift_start_time, shift_end_time ) )
    # print ('Computed duration : %s ' % ( shift_duration ) )
    
    updated_shift_data = {
        'startT': startT,
        'start_time': shift_start_time,
        'endT': endT,
        'end_time': shift_start_time,
        'duration': shift_duration
    }
    return updated_shift_data
    
        
# List/Create
class ShiftListView(generics.ListCreateAPIView):
    
    # permission_classes = [IsAuthenticated]
    
    serializer_class = ShiftSerializer
    
    def get_queryset(self):
        """
        If 'urgent = true' parameter is present in the URL/request
            - Then return non-covered shifts over the next 48 hours
        
        """
        queryset = Shift.objects.all()
        
        userlogin = self.request.query_params.get('userlogin', None)
        urgent = self.request.query_params.get('urgent', False)
        if userlogin is not None:
            print('userlogin=%s' % userlogin )
            # queryset = queryset.filter(userlogin=userlogin)
            # TODO - Use user info to filter shifts (teams)
        else:
            print('userlogin not in the request')
        
        if urgent:
            print('Filtering urgent shifts')
            # Horizon date is +48h
            # TODO - Parameter + take start_time into account
            horizon_date = timezone.now() + datetime.timedelta(hours=48+1)
            
            print('Horizon shift end-date is:' + str(horizon_date))
            queryset = queryset.filter(date__lt=horizon_date)
            queryset = queryset.filter(date__gt=datetime.datetime.today())
        
        return queryset.order_by('date')

    
    # Add a bit of logging to initial
    #.initial(self, request, *args, **kwargs):
    def initial(self, request, *args, **kwargs):
        print ('Got %s request with data : ' % self.request.method)
        print (self.request.data)
        super().initial(request, *args, **kwargs)
        
    
    # Additional create actions
    def perform_create(self, serializer):
        print ("Shift perform_create")
        # print (self.request)
        shift_data = self.request.data
        # shift_data = json.parse(self.request.data)
        print (shift_data)
        # TODO - Validate fields; Shifts should not overlap
        
        # Compute actual date and times
        print ('day %s from %s to %s' % (str(shift_data['day']), str(shift_data['startT']), str(shift_data['endT']) ) )
        # Get parent calendar
        calendar_id = shift_data['calendar_id']
        p_calendar = Calendar.objects.get(pk=calendar_id)
        print ('Parent calendar : %s ; start date is: %s' % ( str(p_calendar), p_calendar.start_date ) )
        shift_date = p_calendar.start_date + datetime.timedelta(days= ( int(shift_data['day']) +-1 ) ) # +1 : 1 in Javascript is Monday (0 in python)
        print ('Computed shift_date : %s ' % ( shift_date ) )
        
        # Compute duration (from date strings)  TODO - Refactor using compute_shift_data
        shift_start_hour = datetime.datetime.strptime(str(shift_data['startT']),"%H") 
        shift_end_hour   = datetime.datetime.strptime(str(shift_data['endT']),"%H") 
        # objects are datetime.datetime
        # print ("shift_start_hour is of type : %s" % str(type(shift_start_hour)))
        
        # Duration (timedelta)
        shift_duration = shift_end_hour - shift_start_hour
        # print ("shift_duration is of type : %s" % str(type(shift_duration)))
        print ('Computed duration : %s ' % ( shift_duration ) )
        
        # Convert to hours (datetime.time)
        shift_start_time = shift_start_hour.time()
        shift_end_time = shift_end_hour.time()
        print ("shift_start_time is of type : %s" % str(type(shift_start_time)))
        print ('Computed start/end times : %s - %s' % ( shift_start_time, shift_end_time ) )
        
        # Populate shift values
        save_kw = { 'date': shift_date, 
                    'start_time': shift_start_time,
                    'end_time': shift_end_time,
                    'duration': shift_duration,
                    'calendar_id': calendar_id,
                  }
        
        # print ('Saving shift:\n' + str(save_kw))
        serializer.save(**save_kw)
        
class ShiftDetailView(generics.RetrieveUpdateDestroyAPIView):
    
    permission_classes = [IsAuthenticated]
    
    queryset = Shift.objects.all()
    serializer_class = ShiftSerializer

    def initial(self, request, *args, **kwargs):
        print ('Got %s request with data : ' % self.request.method)
        print (self.request.data)
        super().initial(request, *args, **kwargs)
    
    def perform_update(self, serializer):
        # print (self.request)
        shift_req_data = self.request.data
        print ("shift_req_data is of type : %s" % str(type(shift_req_data))) # django.http.request.QueryDict
        print ("Shift perform_update :: data : %s" % shift_req_data)
        
        # TODO - Validate fields; Shifts should not overlap
        
        new_shift_kw = serializer.validated_data
        print ("new_shift_kw is of type : %s" % str(type(new_shift_kw))) # collections.OrderedDict
        print ("Validated data :")
        print (new_shift_kw)
        
        cur_shift = self.get_object()
        
        # Compute new startT and endT
        new_startT = new_shift_kw ['startT'] if 'startT' in new_shift_kw else cur_shift.startT
        new_endT = new_shift_kw ['endT'] if 'endT' in new_shift_kw else cur_shift.endT
        print ("Got new shift times: %d - %d" % (new_startT, new_endT) )
        
        updated_shift_data = compute_shift_data(new_startT, new_endT)
        
        print ("Updated shift data :")
        print (updated_shift_data) # debug
        serializer.save(**updated_shift_data)
        
        # TODO - if committments > 0 ==> Send notification
        
    
    
# List/Create
class CommitmentListView(generics.ListCreateAPIView):
    
    permission_classes = [IsAuthenticated]
    
    # queryset = Commitment.objects.all().order_by('-id')
    serializer_class = CommitmentSerializer
    
    def get_queryset(self):
        """
        If 'username' parameter is present in the URL/request,
        return only filtered commitments, ordered by "newest first".
        """
        queryset = Commitment.objects.all() # Default (QuerySets are lazy)
        
        username = self.request.query_params.get('username', None)
        open = self.request.query_params.get('open', False)
        
        if username is not None:
            print('Filtering commitments by username=%s' % username )
            queryset = queryset.filter(username=username)
        else:
            print('username not in the request')
        
        if open:
            print('Filtering commits to confirm')
            queryset = queryset.filter(confirmed_date__isnull=True)
            # queryset = queryset.filter(shift__date='2020-04-23')
            # Horizon date is tomorrow
            horizon_date = timezone.now() + datetime.timedelta(days=1)
            print('Horizon end-date is:' + str(horizon_date))
            # queryset = queryset.filter(shift__date=datetime.datetime.today())
        
        # all 
        
        return queryset.order_by('-id')

    
    def perform_create(self, serializer):
        print ("Commitment perform_create")
        # print (self.request)
        cmt_data = self.request.data
        # shift_data = json.parse(self.request.data)
        print (cmt_data)
        
        shift_id = cmt_data['shift_id']
        
        # Populate create values
        save_kw = { 'shift_id': shift_id } 

        print ('Saving commitment:\n' + str(save_kw))
        try:
            serializer.save(**save_kw)
        # serializer.save()
        except django.db.utils.IntegrityError:
            print ('Integrity error!')
        

class Commitment_History_ListView(generics.ListAPIView):
        
    serializer_class = CommitmentSerializer
    
    def get_queryset(self):
        """
        If 'username' parameter is present in the URL/request,
        return only filtered commitments, ordered by "newest first".
        """
        
        queryset = Commitment.objects.filter(confirmed_date__isnull=False)
        
        username = self.request.query_params.get('username', None)
        if username is not None:
            print('Filtering commitments by username=%s' % username )
            queryset = queryset.filter(username=username)
        else:
            print('username not in the request')
        
        return queryset.order_by('-id')
    
    
# Manage
class CommitmentDetailView(generics.RetrieveUpdateDestroyAPIView):
    
    # permission_classes = [IsAuthenticated] # TODO
    
    queryset = Commitment.objects.all()
    serializer_class = CommitmentSerializer

    def initial(self, request, *args, **kwargs):
        print ('Got %s request with data : ' % self.request.method)
        print (self.request.data)
        super().initial(request, *args, **kwargs)
    
    
    

#### API Templates (replace '_New_' and uncomment)
    
# List/Create
# class _New_ListView(generics.ListCreateAPIView):
    # queryset = _New_.objects.all().order_by('id')
    # serializer_class = _New_Serializer

# Manage
# class _New_DetailView(generics.RetrieveUpdateDestroyAPIView):
    # queryset = _New_.objects.all()
    # serializer_class = _New_Serializer
        

# class UserDetailsListView(generics.ListCreateAPIView):
    # serializer_class = UserDetailsSerializer
    # queryset = UserDetails.objects.all().order_by('name')

        