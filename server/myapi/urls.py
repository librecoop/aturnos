from django.urls import include, path

from rest_framework import routers

from rest_framework.urlpatterns import format_suffix_patterns

from rest_framework.authtoken import views as rest_views

from . import views

# app_name = 'myapi'

# router = routers.DefaultRouter()
# router.register(r'calendars', views.CalendarViewSet)
# router.register(r'shifts', views.ShiftViewSet)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
# urlpatterns = [
    # path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import ensure_csrf_cookie

urlpatterns = [
    # login
    # url(r'^login', views.login),
    # Custom login
    path('login/', views.login, name='login'),
    # path('login/', views.login, name='login'),
     
    # Default REST Token
    # path('api-token-auth/', rest_views.obtain_auth_token, name='api-token-auth'),
    
    # path('calendars/', views.CalendarListView.as_view()),
    path('calendars/', views.CalendarListView.as_view(), name='calendar-list'),
    path('calendars/<int:pk>/', views.CalendarDetailView.as_view(), name='calendar-dtl'),
    
    path('shifts/', views.ShiftListView.as_view(), name='shift-list'),
    path('shifts/<int:pk>/', views.ShiftDetailView.as_view(), name='shift-dtl'),
    
    path('commitments/', views.CommitmentListView.as_view(), name='commitment-list'),
    path('commitments/history/', views.Commitment_History_ListView.as_view(), name='commitment-history'),
    path('commitments/<int:pk>/', views.CommitmentDetailView.as_view(), name='commitment-dtl'),

    


    # path('users/', ensure_csrf_cookie(views.UserAPIView.as_view()), name='user-list'),
    path('users/', views.UserAPIView.as_view(), name='user-list'),
    
    # path('change_password/<int:pk>/', csrf_exempt(views.ChangePasswordAPIView.as_view()), name='change-password'),
    path('change_password/<int:pk>/', views.ChangePasswordAPIView.as_view(), name='change-password'),
    
]

urlpatterns = format_suffix_patterns(urlpatterns)

