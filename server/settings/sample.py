
# ==========================================================
# Sample file to define env-specific settings
# - Debug settings
# - Database
# - ...
#
# To use this environment simply set the appropriate value
# in settings/default.py file:
#   env_settings = 'settings.sample'
# ==========================================================

from settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Override database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'sctest',
        'USER': 'admin1',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}
