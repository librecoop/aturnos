
# Aturnos
_Collaborative shifts app_

## Local Setup Instructions

Default setup and settings are prepared for local development - better in a Linux machine :)

### Backend (API)

0. (Optional) Set up a python3 virtual environment
```shell
virtualenv -p python3 my_env
```
Note: (Windows users might prefer to use **Conda** for this)

1. Activate python venv
```shell
source my_env/bin/activate
```

2. Gather python requirements
```shell
cd server
pip install -r requirements.txt
```

3. Setup database (SQLLite by default)
```shell
python manage.py makemigrations myapi
python manage.py migrate
```

4. Start Django server
```shell
cd server
python manage.py runserver
```

5. (Optional) Load sample data
```shell
python myapi/defaults.py
```



### Frontend (React)

Prepare client:
```shell
cd client
npm install
```

#### Dev mode:
- Served by React develpment server

```shell
cd client
npm start
```
Will run in http://localhost:4000


#### Prod/Test mode
- Served as static files by django

Build with:
```shell
cd client
npm run build
```

Will run in http://localhost:8000

- Make sure your django (dev) server is up and running to reach the database.


## Advanced Setup

### Use custom environment settings

- Create a new file in `server/settings` folder and add/override env-specific contents:
    - You can use **`sample.py`** as an example (copy and rename it to `new.py` for instance)
- Point to your new environment by editing file **`settings/default.py`**, in this example:

```python
...
# Define the environment settings file to use here
env_settings = 'settings.new'
...
```
- Restart Django server

Custom settings files and `default.py` files are kept in git ignore list.

### Use PostgreSQL Database
0. Install PostgreSQL and setup database
1. Get `psycopg2` driver (in your python **venv**):
```shell
pip install psycopg2
```
2. Setup database details **in your custom env settings** (like `settings/new.py`):

```python
...

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'sctest',
        'USER': 'admin',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '',
    }
}

...

```
3. Create tables
```shell
cd server
python manage.py makemigrations myapi
python manage.py migrate
```

4. (re)Start Django server
```shell
python manage.py runserver
```

### Create administration user and roles

```shell
python manage.py createsuperuser
```




## Interesting readings:

Notes for Django+React server setup:
- https://dev.to/shakib609/deploy-your-django-react-js-app-to-heroku-2bck

Ideas on django project folder structure:
- https://stackoverflow.com/questions/22841764/best-practice-for-django-project-working-directory-structure

CSRF:
- https://www.techiediaries.com/django-react-forms-csrf-axios/

DRF permissions with Token:
- http://polyglot.ninja/django-rest-framework-authentication-permissions/

